export class Location{

	constructor(
		public id:number,
		public description:string,
		public parent_id:string,
		public category_id:string
	){}
}