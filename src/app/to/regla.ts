export class Regla{

	constructor(
		public id:number,
		public init_date:string,
		public minimun:string,
		public maximum:string,
		public percentage:string,
		public trm:string,
		public page_id:string,
		public model_id:string, 
		public sede_id:string
	){}
}