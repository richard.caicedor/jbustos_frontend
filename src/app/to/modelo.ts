export class Modelo{

	constructor(
		public id:string,
		public identification:string,
		public name:string,
		public lastname:string,
		public address:string,
		public email:string,
		public phone:string,
		public cellphone:string,
                public admission_date:string,
                public birth_date:string,
                public sede_id:string,
                public birth_country:string,
                public birth_city:string,
                public eps_id:string,
                public afp_id:string,
                public ccf_id:string,
                public country_id:string,
                public city_id:string,
                public status_id:string,
                public category_id:string,
                public fdp_id:string,
                public banco_id:string,
                public cuenta:string,
                public propietario:string,
                public rut:string
	){

	}
}