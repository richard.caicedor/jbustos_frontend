export class Estudio{
	constructor(
		public id:number,
		public name:string,
		public representative:string,
		public contact_phone:string
	){}
}