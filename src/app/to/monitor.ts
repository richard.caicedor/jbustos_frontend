export class Monitor{

	constructor(
		public id:string,
		public identification:string,
		public name:string,
		public lastname:string,
		public address:string,
		public email:string,
		public cellphone:string,
        public admission_date:string,
        public eps_id:string,
        public afp_id:string,
        public ccf_id:string,
        public city_id:string,
        public status_id:string,
        public properties:string,
        public fdp_id:string,
        public category_id:string,
	){

	}
}