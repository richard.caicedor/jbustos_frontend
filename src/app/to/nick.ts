export class Nick{

	constructor(
		public id:string,
		public nickname:string,
		public password:string,
		public url:string,
		public page_id:string
	){}
}