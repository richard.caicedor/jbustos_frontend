export class Categoria{

	constructor(
		public id:number,
		public description:string,
		public type:string
	){}
}