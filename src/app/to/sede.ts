export class Sede{

	constructor(
		public id:number,
		public name:string,
		public representative:string,
		public phone:string,
		public email:string,
		public type:string,
		public address:string,
		public banco_id:string,
		public cuenta:string,
		public nit:string,
		public titular:string,
		public tipo_pago:string,
		public moneda:string,
		public numero:string,
		public email_pago:string
	){}
}