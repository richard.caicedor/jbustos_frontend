export class Pagina {

	constructor(
		public id:number,
		public name:string,
		public url:string,
		public url_login:string,
		public category_id:string,
		public column_nick:string,
		public column_value:string, 
		public column_bonus:string
	){}
} 