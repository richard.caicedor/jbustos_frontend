import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { PersistenceModule } from 'angular-persistence';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FileUploadModule } from 'ng2-file-upload';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AppComponent } from './app.component';
import { appRoutingProviders, routing } from './app.routing';
import { AuthGuard, LoginGuard, PermissionGuard } from './guards';
import { ArlComponent } from './modulos/arl/arl.component';
import { BancosComponent } from './modulos/bancos/bancos.component';
import { BarriosComponent } from './modulos/barrios/barrios.component';
import { CompensacionComponent } from './modulos/cajacompensacion/compensacion.component';
import { DepartamentosComponent } from './modulos/departamentos/departamentos.component';
import { DescuentosComponent } from './modulos/descuentos/descuentos.component';
import { EpsComponent } from './modulos/eps/eps.component';
import { EstudiosComponent } from './modulos/estudios/estudios.component';
import { HomeComponent } from './modulos/estudios/home.component';
import { AdminLayoutComponent } from './modulos/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './modulos/layouts/auth/auth-layout.component';
import { LiquidarComponent } from './modulos/liquidar/liquidar.component';
import { ModelosComponent } from './modulos/modelos/modelos.component';
import { MonitoresComponent } from './modulos/monitores/monitores.component';
import { MunicipiosComponent } from './modulos/municipios/municipios.component';
import { ErrorNicksComponent } from './modulos/nicks/errornicks.component';
import { NicksComponent } from './modulos/nicks/nicks.component';
import { NicksParejasComponent } from './modulos/nicksparejas/nicksparejas.component';
import { PaginasComponent } from './modulos/paginas/paginas.component';
import { PaisesComponent } from './modulos/paises/paises.component';
import { ParejaComponent } from './modulos/parejas/parejas.component';
import { PensionesComponent } from './modulos/pensiones/pensiones.component';
import { RecibosComponent } from './modulos/recibos/recibos.component';
import { ReglasComponent } from './modulos/reglas/reglas.component';
import { SedeDocumentoComponent } from './modulos/sedes/sededocumento.component';
import { SedesComponent } from './modulos/sedes/sedes.component';
import { SharedModule } from './modulos/shared/shared.module';
import { TitleComponent } from './modulos/shared/title/title.component';
import { UploadComponent } from './modulos/upload/upload';
import { ForbiddenComponent } from './modulos/util/forbidden/forbidden.component';
import { NotFoundComponent } from './modulos/util/not-found/not-found.component';
import { VentasComponent } from './modulos/ventas/ventas.component';
import { UsersService } from './services/api';
import { FunctionsService, HttpService, NotifyMessageService, SessionService, SwalMessageService } from './services/util';

// Rutas
@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpModule,
		ReactiveFormsModule,
		routing,
		NgSelectModule,
		HttpClientModule,
		SimpleNotificationsModule.forRoot(),
		NgxPermissionsModule.forRoot(),
		PersistenceModule,
		FileUploadModule,
		AgGridModule.withComponents([]),
		SharedModule,
		PasswordStrengthBarModule
	],
	declarations: [
		AppComponent,
		AuthLayoutComponent,
		AdminLayoutComponent,
		HomeComponent,
		ForbiddenComponent,
		NotFoundComponent,
		TitleComponent,

		// pelle components
		PaginasComponent,
		SedesComponent,
		EpsComponent,
		ArlComponent,
		PensionesComponent,
		BancosComponent,
		ModelosComponent,
		NicksComponent,
		EstudiosComponent,
		PaisesComponent,
		DepartamentosComponent,
		MunicipiosComponent,
		BarriosComponent,
		ReglasComponent,
		CompensacionComponent,
		ParejaComponent,
		NicksParejasComponent,
		UploadComponent,
		MonitoresComponent,
		DescuentosComponent,
		VentasComponent,
		ErrorNicksComponent,
		SedeDocumentoComponent,
		RecibosComponent,
		LiquidarComponent
	],
	providers: [
		appRoutingProviders,
		AuthGuard,
		PermissionGuard,
		LoginGuard,
		HttpService,
		NotifyMessageService,
		SwalMessageService,
		SessionService,
		FunctionsService,
		UsersService
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
