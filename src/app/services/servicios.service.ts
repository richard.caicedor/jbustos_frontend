import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import * as $ from 'jquery';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class Servicio {
	public url: string;

	constructor(public _http: Http) {
		this.url = environment.URL.API;
	}

	getItems(id: string, urlApi: string) {
		$('.loaderBox').show();
		const headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Accept', 'application/json');

		let options = new RequestOptions({ headers: headers });
		let urlConsulta = '';
		if (id != '0') {
			urlConsulta = this.url + urlApi + '/' + id;
		} else {
			urlConsulta = this.url + urlApi;
		}
		return this._http.get(urlConsulta, options).pipe(map(res => res.json()));
	}

	addItem(objeto: any, urlApi: string) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Accept', 'application/json');
		let options = new RequestOptions({ headers: headers });
		return this._http.post(this.url + urlApi, objeto, options).pipe(map(res => res.json()));
	}

	editItem(objeto: any, urlApi: string) {
		let headers = new Headers();	
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Accept', 'application/json');
		let options = new RequestOptions({ headers: headers });
		return this._http.post(this.url + urlApi, objeto, options).pipe(map(res => res.json()));
	}

	loginOn(objeto: any) {
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Accept', 'application/json');
		let options = new RequestOptions({ headers: headers });
		return this._http.post(this.url + 'login', objeto, options).pipe(
			map(res => {
				let token = res.json().token;
				localStorage.setItem('user', token);

				return token != false;
			})
		);
	}
	UploadFile(ruta: string, files, valores) {
		var formData: any = new FormData();
		formData.append('file', files[0]);
		formData.append('fecha_inicio', valores.fecha_inicio);
		formData.append('fecha_fin', valores.fecha_fin);
		formData.append('page_id', valores.page_id);
		this._http.post(this.url + ruta, formData).subscribe(res => {
			console.log(res);
		});
	}
}
