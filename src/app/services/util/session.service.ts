import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PersistenceService, StorageType } from 'angular-persistence';
import { NgxPermissionsService } from 'ngx-permissions';
import { BehaviorSubject } from 'rxjs';
import { GC } from 'src/app/modulos/shared';

interface IUser {
	id: number;
	name: string;
	initials: string;
}

@Injectable()
export class SessionService {
	public get token(): string {
		return this.persistence.get(GC.LS.TOKEN, StorageType.LOCAL) || '';
	}

	user: IUser;
	permissions: string[];
	userReady = new BehaviorSubject<IUser>(null);

	constructor(private router: Router, private ngxPermissionsService: NgxPermissionsService, private persistence: PersistenceService) {}

	/**
	 * logout the current user
	 */
	logout(navigate = true) {
		this.persistence.set(GC.LS.TOKEN, '', { type: StorageType.LOCAL });
		this.user = undefined;
		this.ngxPermissionsService.flushPermissions();
		if (navigate) this.router.navigate(['/auth', 'login']);
	}

	/**
	 * sets the current token
	 */
	setToken(token: string, timestamp: number) {
		this.persistence.set(GC.LS.TOKEN, token, { type: StorageType.LOCAL, expireAfter: timestamp * 1000 });
	}

	/**
	 * sets the current user and permissions
	 */
	setUser(user: IUser, permissions: string[]) {
		user.initials = user.name.substr(0, 1);
		this.user = user;
		this.permissions = permissions;
		this.ngxPermissionsService.loadPermissions(this.permissions);
		this.userReady.next(user);
	}
}
