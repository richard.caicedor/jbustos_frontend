import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

export interface ISwalConfirmationButtons {
  ok?: {
    text?: string;
    handler?: () => void;
    class?: string;
  };
  cancel?: {
    text?: string;
    handler?: (reason: string) => void;
    show?: boolean;
  };
}

const defaults = {
  confirmButtonText: 'Aceptar',
  cancelButtonText: 'Cancelar',
  confirmButtonClass: 'bg-danger',
  cancelButtonClass: 'bg-default'
};

@Injectable()
export class SwalMessageService {
  constructor() {}

  confirmationAjax(title: string, msg: string, buttons: ISwalConfirmationButtons) {
    swal({
      title: title,
      text: msg,
      type: 'question',
      showCancelButton: buttons.cancel && buttons.cancel.show === false ? false : true,
      cancelButtonClass: defaults.cancelButtonClass,
      confirmButtonClass: buttons.ok && buttons.ok.class ? buttons.ok.class : defaults.confirmButtonClass,
      confirmButtonText: buttons.ok && buttons.ok.text ? buttons.ok.text : defaults.confirmButtonText,
      cancelButtonText: buttons.cancel && buttons.cancel.text ? buttons.cancel.text : defaults.cancelButtonText,
      showLoaderOnConfirm: true,
      allowOutsideClick: false,
      preConfirm: () => {
        return new Promise(function(resolve, reject) {
          if (buttons.ok && buttons.ok.handler) buttons.ok.handler();
        });
      }
    }).then(
      ok => {},
      reason => {
        if (buttons.cancel && buttons.cancel.handler) buttons.cancel.handler(reason);
      }
    );
  }

  confirmation(title: string, msg: string, buttons: ISwalConfirmationButtons) {
    swal({
      title: title,
      text: msg,
      type: 'question',
      showCancelButton: buttons.cancel && buttons.cancel.show === false ? false : true,
      cancelButtonClass: defaults.cancelButtonClass,
      confirmButtonClass: buttons.ok && buttons.ok.class ? buttons.ok.class : defaults.confirmButtonClass,
      confirmButtonText: buttons.ok && buttons.ok.text ? buttons.ok.text : defaults.confirmButtonText,
      cancelButtonText: buttons.cancel && buttons.cancel.text ? buttons.cancel.text : defaults.cancelButtonText
    }).then(
      ok => {
        if (buttons.ok && buttons.ok.handler) buttons.ok.handler();
      },
      reason => {
        if (buttons.cancel && buttons.cancel.handler) buttons.cancel.handler(reason);
      }
    );
  }

  success(title: string, msg: string, okHandler?: () => void) {
    swal(title, msg, 'success').then(() => {
      if (okHandler) okHandler();
    });
  }

  warning(title: string, msg: string) {
    swal(title, msg, 'warning');
  }

  error(title: string, msg: string, okHandler?: () => void) {
    swal(title, msg, 'error').then(() => {
      if (okHandler) okHandler();
    });
  }

  info(title: string, msg: string) {
    swal(title, msg, 'info');
  }

  close() {
    swal.close();
  }
}
