import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { NgxPermissionsService } from 'ngx-permissions';
import { Observable } from 'rxjs';

import { NotifyMessageService } from '../util/notify-message.service';

export enum Mod {
	Create = 1,
	Edit = 2,
	View = 3,
	Delete = 4
}

export declare type AutocompleteTypeahead = (query: string, data?: any) => Observable<any>;

export interface IFileData {
	name: string;
	mime: string;
	size: number;
	ext: string;
	base64: string;
}

export interface IDtMessages {
	emptyMessage: string;
	totalMessage: string;
}

@Injectable()
export class FunctionsService {
	dtMessages: IDtMessages = {
		emptyMessage: 'Sin datos para mostrar',
		totalMessage: 'Total'
	};

	public photoTypes = ['image/jpeg', 'image/png', 'image/bmp'];
	public fileTypes = this.photoTypes.concat(['application/pdf']);
	public mailTypes = this.fileTypes.concat([
		'text/plain',
		'application/msword',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'application/vnd.openxmlformats-officedocument.presentationml.slideshow'
	]);
	public photoMaxSize = 4;
	public fileMaxSize = 10;

	public readonly dateTimeFormat = {
		long: 'MMMM D YYYY, h:mm A',
		short: 'MMM D YYYY, h:mm A'
	};

	constructor(private notify: NotifyMessageService, private permissionsService: NgxPermissionsService) {}

	/**
	 * Retorna un array solo con los valores de las filas que cumplan la validacion booleana verdadera
	 * @param array Contenido a comparar
	 * @param column Columna que contiene el valor a retornar
	 * @param checkColumn Columna que contiene el valor a comparar
	 */
	arrayCheck(array: any[], column: string, checkColumn: string) {
		const temp: any[] = [];
		array.forEach(item => {
			if (item[checkColumn] === true) temp.push(item[column]);
		});
		return temp;
	}

	/**
	 * Retorna la diferencia de dos arrays (agregados y eliminados)
	 * @param initial Array inicial
	 * @param final Array final
	 * @deprecated use `Array.difference` instead
	 */
	arrayDifference(initial: any[], final: any[]) {
		initial = initial || [];
		final = final || [];

		return {
			added: final.filter(item => {
				return !initial.includes(item);
			}),
			removed: initial.filter(item => {
				return !final.includes(item);
			})
		};
	}

	randomId() {
		return (
			'_' +
			Math.random()
				.toString(36)
				.substr(2, 9)
		);
	}

	lightenColor(color: string, percent: number) {
		const num = parseInt(color, 16),
			amt = Math.round(2.55 * percent),
			R = (num >> 16) + amt,
			B = ((num >> 8) & 0x00ff) + amt,
			G = (num & 0x0000ff) + amt;

		return (0x1000000 + (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000 + (B < 255 ? (B < 1 ? 0 : B) : 255) * 0x100 + (G < 255 ? (G < 1 ? 0 : G) : 255))
			.toString(16)
			.slice(1);
	}

	objectToQueryParams(data: any) {
		const params: string[] = [];
		Object.keys(data).forEach(key => params.push(key + '=' + data[key]));
		return params.join('&');
	}

	queryParamsToObject(queryParams: string) {
		return JSON.parse('{"' + queryParams.replace(/&/g, '","').replace(/=/g, '":"') + '"}', (key, value) => {
			return key === '' ? value : decodeURIComponent(value);
		});
	}

	/**
	 * Calcula la edad basado en una fecha dada
	 * @param d fecha en formato ISO
	 */
	calculateAge(d: string): number {
		if (!/^(\d{4})-(\d{1,2})-(\d{1,2})$/.test(d)) return null;

		const date = moment(d, 'YYYY-MM-DD').toDate();
		const today = new Date();
		let age = today.getFullYear() - date.getFullYear();
		const m = today.getMonth() - date.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < date.getDate())) age--;
		return age < 0 ? null : age;
	}

	/**
	 * Calcula la edad en meses de una fecha dada
	 * @param d fecha en formato ISO
	 */
	calculateAgeMonths(d: string): number {
		if (!/^(\d{4})-(\d{1,2})-(\d{1,2})$/.test(d)) return null;

		let months: number;
		const date = moment(d, 'YYYY-MM-DD').toDate();
		const today = new Date();
		months = (today.getFullYear() - date.getFullYear()) * 12;
		months -= date.getMonth() + 1;
		months += today.getMonth();
		return months <= 0 ? 0 : months;
	}

	formatNumber(x: number): string {
		const number = x.toString().replace('.', ',');
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
	}

	dateToPickerTime(date: string, format: string = null) {
		if (!date) return null;
		const momentDate = moment(date, format);
		return {
			hour: momentDate.hour(),
			minute: momentDate.minute(),
			second: momentDate.second()
		};
	}

	timePickerToDate(date) {
		if (!date) return null;
		return moment()
			.hour(date.hour)
			.minute(date.minute)
			.format('HH:mm');
	}

	/**
	 * Gets the base64 from a input file
	 * @param e Event from input File
	 * @param validTypes valid mime types for validation
	 * @param maxSize maximum MB size for validation
	 * @param onLoad function to be called when the File has been finished the encoding to base64
	 */
	base64FileInput(e, validTypes: string[], maxSize: number, onLoad: (file: IFileData) => void) {
		if (!e.target.files.length) return;

		const file: File = e.target.files[0];

		if (!validTypes.includes(file.type)) {
			this.notify.error('validation.msgTitle', 'validation.invalidFileType');
			return;
		} else if (file.size > this.megaBytesToBytes(maxSize)) {
			this.notify.error('validation.msgTitle', `El tamaño máximo permitido es ${maxSize} MB`);
			return;
		}

		this.base64Blob(file, onLoad);
		e.target.value = null;
	}

	private base64Blob(file: File, onLoad: (file: IFileData) => void) {
		const reader = new FileReader();
		reader.addEventListener('load', () => {
			onLoad.call(this, {
				name: file.name,
				mime: file.type,
				size: file.size,
				ext: file.name.includes('.') ? file.name.split('.').pop() : null,
				base64: reader.result
			});
		});
		reader.readAsDataURL(file);
	}

	megaBytesToBytes(mb: number) {
		return mb * 1024 * 1024;
	}

	randomUrlKey() {
		return '?r=' + new Date().getTime();
	}

	/**
	 * Convert string date to NgbDateStruct
	 * @param date string
	 * @returns NgbDateStruct
	 */
	dateToPickerDate(date?: string, format?: string) {
		let m: any;
		if (date) m = moment(date, format || 'YYYY-MM-DD');
		else m = moment();
		return {
			year: m.year(),
			month: m.month() + 1,
			day: m.date()
		};
	}

	/**
	 * Add headers to html deleting link reference (a)
	 * @param body string
	 */
	removeHtmlLinks(body: string) {
		return `<!doctype html><html><head><style>a { pointer-events: none; cursor: default; }</style></head><body>${body}</body></html>`;
	}

	/**
	 * Open PDF in new tab from base64 encoded string
	 * @param encodedFile string base64
	 */
	viewPDF(encodedFile: string) {
		if (window.navigator && window.navigator.msSaveOrOpenBlob) {
			const blob = this.generateBlob(encodedFile, 'application/pdf');
			window.navigator.msSaveOrOpenBlob(blob);
		} else {
			const url = this.generateBlobUrl(encodedFile, 'application/pdf');
			window.open(url, '_blank');
		}
	}

	/**
	 * Generate blob URL to BASE64 file
	 * @param base64File string
	 * @param mime string
	 * @param returnBlob boolean
	 */
	generateBlobUrl(base64File: string, mime: string, returnBlob?: boolean) {
		const blob = this.generateBlob(base64File, mime);
		return URL.createObjectURL(blob);
	}

	/**
	 * Generate blob URL to BASE64 file
	 * @param base64File string
	 * @param mime string
	 */
	generateBlob(base64File: string, mime: string) {
		const byteCharacters = atob(base64File);
		const byteNumbers = new Array(byteCharacters.length);
		for (let i = 0; i < byteCharacters.length; i++) {
			byteNumbers[i] = byteCharacters.charCodeAt(i);
		}
		const byteArray = new Uint8Array(byteNumbers);
		return new Blob([byteArray], { type: mime });
	}

	/**
	 * Download file
	 * @param string encodedFile
	 * @param string mime
	 * @param string ext
	 * @param string section
	 */
	download(encodedFile: string, mime: string, ext: string, fileName?: string) {
		const decodedFileData = atob(encodedFile);
		const byteNumbers = new Array(decodedFileData.length);
		for (let i = 0; i < decodedFileData.length; i++) {
			byteNumbers[i] = decodedFileData.charCodeAt(i);
		}
		const byteArray = new Uint8Array(byteNumbers);
		const blob = new Blob([byteArray], { type: mime });
		const url = window.URL.createObjectURL(blob);
		const a = document.createElement('a');
		document.body.appendChild(a);
		a.href = url;
		a.download = (fileName || moment().format('YYYY-MM-DD')) + '.' + ext;
		a.click();
		window.URL.revokeObjectURL(url);
		a.remove();
	}

	/**
	 * Quitar los acentos de las vocales
	 */
	removeAccents(s: string) {
		if (!s) return s;
		s = s.toLocaleLowerCase();
		return s
			.replace(new RegExp(/[àáâãäå]/g), 'a')
			.replace(new RegExp(/[èéêë]/g), 'e')
			.replace(new RegExp(/[ìíîï]/g), 'i')
			.replace(new RegExp(/[òóôõö]/g), 'o')
			.replace(new RegExp(/[ùúûü]/g), 'u');
	}
}
