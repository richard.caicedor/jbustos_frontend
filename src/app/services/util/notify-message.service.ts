import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Injectable()
export class NotifyMessageService {
	public options = {
		timeOut: 2000,
		showProgressBar: false,
		pauseOnHover: true,
		lastOnBottom: true,
		clickToClose: true,
		animate: 'fromRight',
		position: ['top', 'right']
	};

	constructor(private notify: NotificationsService) {}

	success(title: string, msg: string, opts?: any) {
		this.notify.success(title, msg, opts);
	}

	warning(title: string, msg: string, opts?: any) {
		this.notify.warn(title, msg, opts);
	}

	info(title: string, msg: string, opts?: any) {
		this.notify.info(title, msg, opts);
	}

	error(title: string, msg: string, opts?: any) {
		this.notify.error(title, msg, opts);
	}
}
