import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpService } from '../util';

declare const $;

@Injectable()
export class PagesService {
	public static URL = environment.URL.API + 'pages';

	constructor(private http: HttpService) {}

	/**
	 * Listado de paginas para select
	 */
	list = () => {
		return this.http.observableGet(`${PagesService.URL}/list`, { mapFn: res => res.pages });
	};
}
