import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpRequestHandler, HttpService } from '../util';

@Injectable()
export class MenuService {
	public static URL = environment.URL.API + 'menu';

	constructor(private http: HttpService) {}

	/**
	 * Mostrar listado de menus
	 */
	index(params: string, httpRequestHandler: HttpRequestHandler) {
		this.http.get(MenuService.URL + '?' + params, httpRequestHandler);
	}

	/**
	 * Guardar menu
	 */
	save(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(MenuService.URL, data, httpRequestHandler);
	}

	/**
	 * Eliminar menu
	 */
	delete(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.delete(`${MenuService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Obtener menu
	 */
	get(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${MenuService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Arbol menu
	 */
	tree(httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${MenuService.URL}/tree`, httpRequestHandler);
	}

	/**
	 * Listado de menu para select
	 */
	list = () => {
		return this.http.observableGet(`${MenuService.URL}/list`, { mapFn: res => res.menus });
	};
}
