import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpRequestHandler, HttpRequestObservable, HttpService, MessageType } from '../util';

@Injectable()
export class UsersService {
	static URL = environment.URL.API + 'user';

	constructor(private http: HttpService) {}

	login(data: any, httpRequestHandler: HttpRequestHandler) {
		httpRequestHandler.includeToken = false;
		this.http.post(UsersService.URL + '/login', data, httpRequestHandler);
	}

	logout(httpRequestHandler: HttpRequestHandler) {
		httpRequestHandler.errorMsgType = MessageType.None;
		this.http.get(UsersService.URL + '/logout', httpRequestHandler);
	}

	current(httpRequestHandler: HttpRequestHandler) {
		this.http.get(UsersService.URL + '/current', httpRequestHandler);
	}

	currentObservable(httpRequestObservable: HttpRequestObservable) {
		return this.http.observableGet(UsersService.URL + '/current', httpRequestObservable);
	}

	changePassword(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(UsersService.URL + '/change-password', data, httpRequestHandler);
	}

	/**
	 * Mostrar listado de acciones
	 */
	index(params: string, httpRequestHandler: HttpRequestHandler) {
		this.http.get(UsersService.URL + '?' + params, httpRequestHandler);
	}

	/**
	 * Guardar acción
	 */
	save(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(UsersService.URL, data, httpRequestHandler);
	}

	/**
	 * Eliminar acción
	 */
	delete(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.delete(`${UsersService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Obtener acción
	 */
	get(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${UsersService.URL}/${id}`, httpRequestHandler);
	}
}
