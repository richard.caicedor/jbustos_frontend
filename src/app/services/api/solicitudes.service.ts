import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { FunctionsService, HttpRequestHandler, HttpService } from '../util';

declare const $;

@Injectable()
export class SolicitudesService {
	public static BASE_URL_API = environment.URL.API + 'request';

	constructor(private http: HttpService, private fn: FunctionsService) {}

	/**
	 * Mostrar listado de roles
	 */
	index(params: string, httpRequestHandler: HttpRequestHandler) {
		this.http.get(SolicitudesService.BASE_URL_API + '?' + params, httpRequestHandler);
	}

	/**
	 * Guardar rol
	 */
	save(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(SolicitudesService.BASE_URL_API, data, httpRequestHandler);
	}

	/**
	 * Eliminar rol
	 */
	delete(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.delete(`${SolicitudesService.BASE_URL_API}/${id}`, httpRequestHandler);
	}

	/**
	 * Obtener rol
	 */
	get(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${SolicitudesService.BASE_URL_API}/${id}`, httpRequestHandler);
	}

	approve(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${SolicitudesService.BASE_URL_API}/approve/${id}`, httpRequestHandler);
	}

	deny(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${SolicitudesService.BASE_URL_API}/deny/${id}`, httpRequestHandler);
	}

	comments(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${SolicitudesService.BASE_URL_API}/comments/${id}`, httpRequestHandler);
	}

	saveComment(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(`${SolicitudesService.BASE_URL_API}/saveComment`, data, httpRequestHandler);
	}

	/**
	 * Obtener rol
	 */
	pages(httpRequestHandler: HttpRequestHandler) {
		this.http.get(SolicitudesService.BASE_URL_API + '/pages', httpRequestHandler);
	}

	// /**
	//  * Listado de roles para select
	//  */
	// list(httpRequestHandler: HttpRequestHandler) {
	// 	this.http.get(`${RolesService.BASE_URL_API}/list`, httpRequestHandler);
	// }

	// /**
	//  * Obtener acciones del rol
	//  */
	// actions(id: number, httpRequestHandler: HttpRequestHandler) {
	// 	this.http.get(`${RolesService.BASE_URL_API}/${id}/actions`, httpRequestHandler);
	// }

	// /**
	//  * Guardar acciones del rol
	//  */
	// saveActions(id: number, data: any, httpRequestHandler: HttpRequestHandler) {
	// 	this.http.post(`${RolesService.BASE_URL_API}/${id}/actions`, data, httpRequestHandler);
	// }
}
