import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpRequestHandler, HttpService } from '../util';

declare const $;

@Injectable()
export class RolesService {
	public static URL = environment.URL.API + 'role';

	constructor(private http: HttpService) {}

	/**
	 * Mostrar listado de roles
	 */
	index(params: string, httpRequestHandler: HttpRequestHandler) {
		this.http.get(RolesService.URL + '?' + params, httpRequestHandler);
	}

	/**
	 * Guardar rol
	 */
	save(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(RolesService.URL, data, httpRequestHandler);
	}

	/**
	 * Eliminar rol
	 */
	delete(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.delete(`${RolesService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Obtener rol
	 */
	get(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${RolesService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Listado de roles para select
	 */
	list = () => {
		return this.http.observableGet(`${RolesService.URL}/list`, { mapFn: res => res.roles });
	};

	/**
	 * Obtener acciones del rol
	 */
	actions(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${RolesService.URL}/${id}/actions`, httpRequestHandler);
	}

	/**
	 * Guardar acciones del rol
	 */
	saveActions(id: number, data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(`${RolesService.URL}/${id}/actions`, data, httpRequestHandler);
	}
}
