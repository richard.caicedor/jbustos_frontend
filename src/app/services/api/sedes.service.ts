import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpService } from '../util';

@Injectable()
export class SedesService {
	public static URL = environment.URL.API + 'sedes';

	constructor(private http: HttpService) {}

	/**
	 * Listado de sedes
	 */
	list = () => {
		return this.http.observableGet(`${SedesService.URL}/list`, { mapFn: res => res.sedes });
	};
}
