import { Injectable } from '@angular/core';
import { SelectLookupFn } from 'src/app/modulos/shared/select';
import { environment } from 'src/environments/environment';

import { HttpService } from '../util';

@Injectable()
export class ModelService {
	public static URL = environment.URL.API + 'models';

	constructor(private http: HttpService) {}

	/**
	 * listado async de nicks para select
	 */
	list: SelectLookupFn = data => {
		return this.http.observablePost(`${ModelService.URL}/list`, data, { mapFn: res => res.models });
	};
}
