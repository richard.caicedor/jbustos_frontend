import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpService } from '../util';

@Injectable()
export class StudiosService {
	public static URL = environment.URL.API + 'studios';

	constructor(private http: HttpService) {}

	/**
	 * Listado de sedes
	 */
	list = () => {
		return this.http.observableGet(`${StudiosService.URL}/list`, { mapFn: res => res.studios });
	};
}
