import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpRequestHandler, HttpService } from '../util';

@Injectable()
export class ActionsService {
	public static URL = environment.URL.API + 'action';

	constructor(private http: HttpService) {}
	/**
	 * Mostrar listado de acciones
	 */
	index(params: string, httpRequestHandler: HttpRequestHandler) {
		this.http.get(ActionsService.URL + '?' + params, httpRequestHandler);
	}

	/**
	 * Guardar acción
	 */
	save(data: any, httpRequestHandler: HttpRequestHandler) {
		this.http.post(ActionsService.URL, data, httpRequestHandler);
	}

	/**
	 * Eliminar acción
	 */
	delete(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.delete(`${ActionsService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Obtener acción
	 */
	get(id: number, httpRequestHandler: HttpRequestHandler) {
		this.http.get(`${ActionsService.URL}/${id}`, httpRequestHandler);
	}

	/**
	 * Listado de acciones para select
	 */
	list = () => {
		return this.http.observableGet(`${ActionsService.URL}/list`, { mapFn: res => res.actions });
	};
}
