import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';

import { AuthGuard, LoginGuard } from './guards';
import { ArlComponent } from './modulos/arl/arl.component';
import { BancosComponent } from './modulos/bancos/bancos.component';
import { BarriosComponent } from './modulos/barrios/barrios.component';
import { CompensacionComponent } from './modulos/cajacompensacion/compensacion.component';
import { DepartamentosComponent } from './modulos/departamentos/departamentos.component';
import { DescuentosComponent } from './modulos/descuentos/descuentos.component';
import { EpsComponent } from './modulos/eps/eps.component';
import { EstudiosComponent } from './modulos/estudios/estudios.component';
import { HomeComponent } from './modulos/estudios/home.component';
import { AdminLayoutComponent } from './modulos/layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './modulos/layouts/auth/auth-layout.component';
import { LiquidarComponent } from './modulos/liquidar/liquidar.component';
import { ModelosComponent } from './modulos/modelos/modelos.component';
import { MonitoresComponent } from './modulos/monitores/monitores.component';
import { MunicipiosComponent } from './modulos/municipios/municipios.component';
import { ErrorNicksComponent } from './modulos/nicks/errornicks.component';
import { NicksComponent } from './modulos/nicks/nicks.component';
import { NicksParejasComponent } from './modulos/nicksparejas/nicksparejas.component';
import { PaginasComponent } from './modulos/paginas/paginas.component';
import { PaisesComponent } from './modulos/paises/paises.component';
import { ParejaComponent } from './modulos/parejas/parejas.component';
import { PensionesComponent } from './modulos/pensiones/pensiones.component';
import { RecibosComponent } from './modulos/recibos/recibos.component';
import { ReglasComponent } from './modulos/reglas/reglas.component';
import { SedeDocumentoComponent } from './modulos/sedes/sededocumento.component';
import { SedesComponent } from './modulos/sedes/sedes.component';
import { UploadComponent } from './modulos/upload/upload';
import { ForbiddenComponent } from './modulos/util/forbidden/forbidden.component';
import { NotFoundComponent } from './modulos/util/not-found/not-found.component';
import { VentasComponent } from './modulos/ventas/ventas.component';

const appRoutes: Routes = [
	{
		path: 'auth',
		component: AuthLayoutComponent,
		canActivate: [LoginGuard],
		children: [
			{
				path: '',
				loadChildren: () => import('./modulos/auth/auth.module').then(m => m.AuthModule)
			}
		]
	},
	{
		path: '',
		component: AdminLayoutComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: '',
				component: HomeComponent,
				data: {
					title: 'Home'
				}
			},
			{
				path: 'roles',
				loadChildren: () => import('./modulos/roles/roles.module').then(m => m.RoleModule)
			},
			{
				path: 'solicitudes',
				loadChildren: () => import('./modulos/solicitudes/solicitudes.module').then(m => m.SolicitudModule)
			},
			{
				path: 'paginas',
				component: PaginasComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Administración de Páginas',
					permission: 'pages_view'
				}
			},
			{
				path: 'actions',
				loadChildren: () => import('./modulos/actions/actions.module').then(m => m.ActionsModule)
			},
			{
				path: 'users',
				loadChildren: () => import('./modulos/users/users.module').then(m => m.UsersModule)
			},

			{
				path: 'menu',
				loadChildren: () => import('./modulos/menu/menu.module').then(m => m.MenuModule)
			},
			{
				path: 'estudios',
				component: EstudiosComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Administración de Estudios',
					permission: 'estudios_view'
				} 
			},
			{
				path: 'paises',
				component: PaisesComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Administración de Países',
					permission: 'paises_view'
				} 
			},
			{
				path: 'departamentos',
				component: DepartamentosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Departamentos',
					permission: 'departamentos_view'
				} 
			},
			{
				path: 'municipios',
				component: MunicipiosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Municipios',
					permission: 'municipios_view'
				} 
			},
			{
				path: 'barrios',
				component: BarriosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Barrios',
					permission: 'barrios_view'
				} 
			},
			{
				path: 'sedes',
				component: SedesComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Sedes',
					permission: 'sedes_view'
				} 
			},
 			{
				path: 'eps',
				component: EpsComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración EPS',
					permission: 'eps_view'
				} 
			},
			{
				path: 'arl',
				component: ArlComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración ARL',
					permission: 'eps_view'
				} 
			},
			{
				path: 'compensacion',
				component: CompensacionComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración Caja de Compensación',
					permission: 'cdf_view' 
				} 
			},
			{
				path: 'pensiones',
				component: PensionesComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Fondo de Pensiones',
					permission: 'fdp_view' 
				} 
			},
			{
				path: 'bancos',
				component: BancosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Bancos',
					permission: 'bancos_view' 
				} 
			},
 			{
				path: 'modelos',
				component: ModelosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Modelos',
					permission: 'models_view' 
				} 
			},
			{
				path: 'nicks',
				component: NicksComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Nicks',
					permission: 'nicks_view' 
				} 
			},
			{
				path: 'parejas',
				component: ParejaComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Parejas',
					permission: 'parejas_view' 
				}  
			},
			{
				path: 'monitores',
				component: MonitoresComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Monitores',
					permission: 'monitores_view' 
				}  
			}, 
			{
				path: 'nicks-parejas',
				component: NicksParejasComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Nicks Parejas',
					permission: 'nicks_parejas_view' 
				}  
			}, 
			{
				path: 'reglas',
				component: ReglasComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Reglas',
					permission: 'reglas_view' 
				}  
			}, 
			{
				path: 'ventas',
				component: VentasComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Ventas',
					permission: 'ventas_view' 
				}  
			}, 
			{
				path: 'liquidar',
				component: LiquidarComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Proceso de Liquidación',
					permission: 'liquidar_view' 
				}  
			},   
			{
				path: 'descuentos',
				component: DescuentosComponent,
				canActivate: [PermissionGuard],
				data: { 
					title: 'Administración de Descuentos',
					permission: 'descuentos_view' 
				}  
			},   





			{ path: 'nicks_error', component: ErrorNicksComponent },
			{ path: 'sede_document', component: SedeDocumentoComponent },
			{ path: 'recibos_nomina', component: RecibosComponent },




			{
				path: 'forbidden',
				component: ForbiddenComponent
			},
			{
				path: 'not-found',
				component: NotFoundComponent
			},
			{
				path: '**',
				component: NotFoundComponent
			}
		]
	}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
