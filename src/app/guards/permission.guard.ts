import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { UsersService } from '../services/api';
import { SessionService } from '../services/util';

@Injectable()
export class PermissionGuard implements CanActivate {
	constructor(private userService: UsersService, private sessionService: SessionService, private router: Router) {}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> {
		// doing nothing when the current route has no permission to validate
		const permission: string = next.data.permission;
		if (!permission) return true;

		// function that checks the permission for the current route
		const checkPermisssion = () => {
			if (this.sessionService.permissions.includes(permission)) return true;
			else {
				this.router.navigate(['/forbidden'], { queryParams: { permission: permission, url: encodeURIComponent(state.url) } });
				return false;
			}
		};

		// check the permission if the user's permissions are loaded
		if (this.sessionService.permissions) return checkPermisssion();

		// load the user's permissions and check the permission
		return this.userService
			.currentObservable({
				mapFn: res => {
					this.sessionService.setUser(res.user, res.actions);
					return checkPermisssion();
				}
			})
			.pipe(
				catchError((err, obs) => {
					this.sessionService.logout(false);
					this.router.navigate(['/auth', 'login'], { queryParams: { returnUrl: encodeURIComponent(state.url) } });
					return of(false);
				})
			);
	}
}
