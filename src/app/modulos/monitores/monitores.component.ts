import {Component, OnInit } from '@angular/core';
import { Monitor } from '../../to/monitor';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

const formData = new FormData()

@Component({
	selector: 'app-monitores',
	templateUrl: './monitores.component.html',
	providers: [Servicio]
})

export class MonitoresComponent implements OnInit{

	public modelos:any[];
	public monitores:Monitor[]; 
	public monitorForm:FormGroup;
	public modelMonitorForm:FormGroup;
	public modelMonitors:any[];
	public textbtn:string;
	public eps:any[];
	public arl:any[];
	public fp:any[];
	public cdf:any[];
	public ciudades:any[];
	public submitted:boolean;
	public id_monitor:string;

	constructor(public _modeloService:Servicio){
		this.textbtn ='Guardar';
		this.monitorForm  = this.createFormGroup();
		this.modelMonitorForm = this.createFormGroupModelMonitor();
		this.submitted = false; 
	}

	createFormGroupModelMonitor(){
		return new FormGroup({
			id: new FormControl(''),
			initial_date: new FormControl('',[Validators.required]),
			model_id: new FormControl('',[Validators.required]),
			monitor_id: new FormControl(''),
			percentage: new FormControl('',[Validators.required]),
		}); 
	}

	onToggleForm(){
		this.onResetForm();
		$('#divForm').toggle(200);
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			identification: new FormControl('',[Validators.required]),
			name: new FormControl('',[Validators.required]),
			lastname: new FormControl('',[Validators.required]),
			address: new FormControl('',[Validators.required]),
			email: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			cellphone: new FormControl('',[Validators.required, Validators.maxLength(10)]),
			admission_date: new FormControl('',[Validators.required]),
			eps_id: new FormControl('',[Validators.required]),
			afp_id: new FormControl('',[Validators.required]),
			ccf_id: new FormControl('',[Validators.required]),
			fdp_id: new FormControl('',[Validators.required]),
			city_id: new FormControl('',[Validators.required]),
		}); 
	}

	onResetForm(){
		this.textbtn ='Guardar';
		this.monitorForm.reset();
		this.modelMonitorForm.reset();
		this.monitorForm.controls['id'].setValue("");
	}

	onEditar(monitor:Monitor){
		this.textbtn ='Modificar';
		this.monitorForm.controls['id'].setValue(monitor.id);
		this.monitorForm.controls['identification'].setValue(monitor.identification);
		this.monitorForm.controls['name'].setValue(monitor.name);
		this.monitorForm.controls['lastname'].setValue(monitor.lastname);
		this.monitorForm.controls['email'].setValue(monitor.email);
		this.monitorForm.controls['cellphone'].setValue(monitor.cellphone);
		this.monitorForm.controls['admission_date'].setValue(monitor.admission_date);
		this.monitorForm.controls['city_id'].setValue(monitor.city_id);
		this.monitorForm.controls['address'].setValue(monitor.address);
		this.monitorForm.controls['eps_id'].setValue(monitor.eps_id);
		this.monitorForm.controls['afp_id'].setValue(monitor.afp_id);
		this.monitorForm.controls['ccf_id'].setValue(monitor.ccf_id);
		this.monitorForm.controls['fdp_id'].setValue(monitor.fdp_id);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}
	
	onSaveForm(){
		this.submitted = true;
		if(this.monitorForm.get('id').value !== ''){
			if(!this.monitorForm .invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._modeloService.editItem(this.monitorForm.value,'monitores/editmonitor').subscribe(
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}
		} else{  
			if(!this.monitorForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._modeloService.addItem(this.monitorForm.value,'monitores/monitor').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      },  
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	onSaveModeloMonitor(){
		if(!this.modelMonitorForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this.modelMonitorForm.controls['monitor_id'].setValue(this.id_monitor);  
				this._modeloService.addItem(this.modelMonitorForm.value,'monitores/modelosMonitor').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			        this.getModelosMonitor();
			        this.onToggleForm();
			      },  
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
	}


	onEliminar(id:string){

		if(confirm('¿Desea eliminar el registro?')){
			this._modeloService.getItems(id,'monitores/deletemonitor').subscribe(
				result => { 
					$('.loaderBox').hide();  
					this.getListas();
				},
				error => {
					console.log(<any>error);
				} 
			); 
		}
	}

	onEliminarMM(id:string){
		if(confirm('¿Desea eliminar el registro?')){
			this._modeloService.getItems(id,'monitores/modelosMonitorDelete').subscribe(
				result => { 
					$('.loaderBox').hide();  
					this.getModelosMonitor();
				},
				error => {
					console.log(<any>error);
				} 
			); 
		}
	}

	ngOnInit(){
		this.getListas();
	}

	cargarDatos(monitor:Monitor){
		this.id_monitor = monitor.id;
		this.getModelosMonitor();
	} 

	getModelosMonitor(){
		this._modeloService.getItems(this.id_monitor,'monitores/modelosMonitor').subscribe(
			result => { 
				this.modelMonitors = result.datos; 
				$('.loaderBox').hide();
			}, 
			error => {
				console.log(<any>error);
			}
		); 
	}

	getListas(){
		this._modeloService.getItems('','monitores/monitor').subscribe(
			result => { 
				this.modelos = result.modelos;
				this.monitores = result.monitores;
				this.eps = result.eps;
				this.arl = result.arl;
				this.fp = result.fp;
				this.ciudades = result.ciudades;
				this.cdf = result.cdf;
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				});

				$('.loaderBox').hide();
			}, 
			error => {
				console.log(<any>error);
			}
		); 

		
	}

	get f() { return this.monitorForm.controls; }
	get g() { return this.modelMonitorForm.controls; }

}