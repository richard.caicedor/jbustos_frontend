import {Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid-community";
import {Servicio} from '../../services/servicios.service';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({  
    selector: 'app-descuentos',
    templateUrl: './descuentos.component.html',
    providers: [Servicio]
})

export class DescuentosComponent implements OnInit {
    
    public gridOptions:GridOptions; 
    public sedes:any[];
    public consultaForm:FormGroup;
    public submitted:boolean;
    public btnSubir:boolean; 
    public filesToUpload;
    public fileForm:FormGroup;

    constructor(public _servicio:Servicio,private notify: NotifyMessageService){
        this.gridOptions = <GridOptions>{
            //rowData: DescuentosComponent.Datos(),
            columnDefs: DescuentosComponent.Columnas(),
            context: {
                componentParent: this
            },
            components: {
                'FormatoCelda': this.FormatoCelda
            }
        };
        this.consultaForm  = this.createFormConsulta();
        this.fileForm  = this.createFormGroupFile();
        this.submitted = false; 
    }

    private createFormConsulta(){   
        return new FormGroup({
            sede_id: new FormControl(null,[Validators.required]),
            fecha_inicio: new FormControl(null,[Validators.required]),
            fecha_fin: new FormControl(null,[Validators.required]),
        }); 
    }

    createFormGroupFile(){
        return new FormGroup({
            file: new FormControl('',[Validators.required]),
            fecha_inicio: new FormControl('',[Validators.required]),
            fecha_fin: new FormControl('',[Validators.required]),
        }); 
    }

    private static Columnas(){
        return [
            {headerName: 'Id', field: 'model_id', pinned: 'left', hide: "true"},
            {headerName: 'Identificación', field: 'identificacion', pinned: 'left'},
            {headerName: 'Nombre', field: 'nombre' , pinned: 'left'},
            {headerName: 'Arriendo', field: 'arriendo', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Internet', field: 'internet', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }}, 
            {headerName: 'Servicios', field: 'servicio', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Borrar Videos', field: 'borratusvideos', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Prestamos', field: 'prestamos', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'EPS', field: 'eps', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Descuentos', field: 'descuentos', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Premios', field: 'premios', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
            {headerName: 'Observación', field: 'observacion', editable: true},
        ];
    }

    public ChangeCargarArchivo(fileInput:any){
        this.filesToUpload = <Array<File>>fileInput.target.files; 
    }

    private static Datos(){
        /*return [
            { model_id: 1 ,identificacion: '123456', nombre: 'Andrea Benites' },
        ];*/ 
    }    


    public onConsulta(){
        this.submitted = true;
        
        if(!this.consultaForm.invalid){
            $('.loaderBox').show();
            this._servicio.addItem(this.consultaForm.value,'descuentos/getmodeldiscount').subscribe(
                result => { 
                    $('.loaderBox').hide();
                    this.gridOptions.api.setRowData(result);
                    $('#detalleConsulta').show(300);
                }, 
                error => {
                    console.log(<any>error);
                }
            );
        }
    }

    ngOnInit(){
        this.getListas();
    }

    private FormatoCelda(params:any) {
        var usdFormate = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 1
        }); 
        return usdFormate.format(params.value);
    }
  
    private OnGuardarDatos() {
      let rowData = [];
      this.gridOptions.api.forEachNode(node => rowData.push(node.data));
      rowData.push(this.consultaForm.value);
      $('.loaderBox').show();
      this._servicio.addItem(rowData,'descuentos/modeldiscount').subscribe(
        response => {
            if(response.status !== 'Ok'){
                    this.notify.error('Error', 'Error al guardar Descuentos!');
            }else{ 
                this.notify.success('Éxito', 'Descuentos Almacenados Éxitosamente!');
            }
            $('.loaderBox').hide();
        }, 
        error => { 
            console.log(<any>error);
        } 
        ); 
    }
 
    getListas(){
        this._servicio.getItems('0','sedes/sede').subscribe(
            result => { 
                this.sedes = result.sedes;
                $('.loaderBox').hide();
            }, 
            error => {
                console.log(<any>error);
            }
        );
    }

    onEnviarArchivo(){  
        this.btnSubir = true; 
        if(!this.fileForm.invalid){
            console.log(this.filesToUpload);
            console.log(this.fileForm.value); 
            this.onResetForm();
            $('#btnCerrarModal').click();
            /*
            this._servicio.UploadFile('', this.filesToUpload, this.fileForm.value);
            $('#btnCerrarModal').click();
            $('#alertMessage').html('<b>Carga de Ventas Éxitosa!</b>');
            $('#alertMessage').show();
            setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
            */
        }
        
    }

    onResetForm(){
        this.fileForm.reset();
    }

    get f() { return this.consultaForm.controls; }
    get g() { return this.fileForm.controls; }
}