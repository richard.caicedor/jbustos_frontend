import {Component, OnInit } from '@angular/core';
import { Pareja } from '../../to/pareja';
import { Nick } from '../../to/nick';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-nicks',
	templateUrl: './nicksparejas.component.html',
	providers: [Servicio]
})

export class NicksParejasComponent implements OnInit{

	public parejas:any[]; 
	public pareja:Pareja; 
	public nicks:Nick[];
	public nickForm:FormGroup;
	public textbtn:string;
	public submitted:boolean;
	public nombre:string;
	public paginas:any[];

	constructor(public _service:Servicio){
		this.textbtn ='Guardar';
		this.nickForm  = this.createFormGroup();
		this.submitted = false; 
	}
	
	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			nickname: new FormControl('',[Validators.required]),
			password: new FormControl('',[Validators.required]),
			url: new FormControl('',[Validators.required]),
			page_id: new FormControl(null,[Validators.required]),
			model_id: new FormControl('',[Validators.required])
		}); 
	} 

	onToggleForm(){
		this.onResetForm();
		$('#divForm').toggle(200);
	}
	
	onResetForm(){
		this.textbtn ='Guardar';
		this.nickForm.reset();
		this.nickForm.controls['id'].setValue("");
		this.nickForm.controls['model_id'].setValue(this.pareja.id); 
	}

	onEditar(nick:Nick){
		$('#divForm').show(200);
		this.textbtn ='Modificar';
		this.nickForm.controls['id'].setValue(nick.id);
		this.nickForm.controls['nickname'].setValue(nick.nickname);
		this.nickForm.controls['password'].setValue(nick.password);
		this.nickForm.controls['url'].setValue(nick.url);
		this.nickForm.controls['page_id'].setValue(nick.page_id); 
	}

	resultApi(msg:string){
		this.onToggleForm();
		$('#btnGuardar').removeAttr('disabled');
		this.submitted = false;
	}
	
	onSaveForm(){
		this.submitted = true;
		if(this.nickForm.get('id').value !== ''){
			if(!this.nickForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.editItem(this.nickForm.value,'nicks_parejas/editcouplenick').subscribe(
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      	this.onResetForm();
			        this.getListaNicks(); 
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}
		} else{
			if(!this.nickForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.addItem(this.nickForm.value,'nicks_parejas/couplenick').subscribe(
			      response => { 
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			        this.getListaNicks(); 
			      },   
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	cargarDatos(pareja:any){
		this.pareja = pareja;
		this.nombre = pareja.modeloinfo.name + ' '+ pareja.modeloinfo.lastname + ' / '+pareja.pareja.name + ' '+ pareja.pareja.lastname  
		this.nickForm.controls['model_id'].setValue(pareja.id);  
		this.getListaNicks(); 
	} 
  
	ngOnInit(){
		this.getListas();
	} 

	getListaNicks(){
		this._service.getItems(this.pareja.id,'nicks_parejas/nickParejas').subscribe(
			result => { 
				this.nicks = result;
				$('.loaderBox').hide();  
			},
			error => {
				console.log(<any>error);
			} 
		); 
	} 

	onEliminar(nick:Nick){
		this._service.getItems(nick.id,'nicks_parejas/nickdelete').subscribe(
			result => { 
				$('.loaderBox').hide(); 
				this.getListaNicks(); 
			},
			error => {
				console.log(<any>error);
			} 
		); 
	}

	getListas(){
		this._service.getItems('','parejas/parmodelos').subscribe( 
			result => { 
				this.paginas = result.paginas;
				this.parejas = result.parejas;
				$('.loaderBox').hide(); 
			},
			error => {
				console.log(<any>error);
			} 
		);  
	}

	get f() { return this.nickForm.controls; }
}