import {Component, OnInit } from '@angular/core';
import {Location} from '../../to/location';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-arl',
	templateUrl: './paises.component.html',
	providers: [Servicio]
}) 

export class PaisesComponent implements OnInit{

	public items:Location[];
	public _formGroup:FormGroup;
	public textbtn:string;
	public modulo:string;
	public submitted:boolean;
	public lblDependencia:string;
	public selectItems:any[];
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.modulo = " Países";
		this._formGroup = this.createFormGroup();
		this.submitted = false;
		this.lblDependencia= null;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
			category_id: new FormControl(''),
			parent_id: new FormControl('')
		}); 
	} 

	onResetForm(){  
		this.textbtn ='Guardar';
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
	} 

	onEditar(item:Location){
		this.textbtn ='Modificar';
		this._formGroup.controls['id'].setValue(item.id);
		this._formGroup.controls['description'].setValue(item.description);
		this._formGroup.controls['category_id'].setValue(15);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true; 
		this._formGroup.controls['parent_id'].setValue(null);
		if(this._formGroup.get('id').value !== ''){
			if(!this._formGroup.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this._formGroup.value,'locations/editlocation').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			
			if(!this._formGroup.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._formGroup.controls['category_id'].setValue(15); 
				this._InformacionService.addItem(this._formGroup.value,'locations/location').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	} 

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._InformacionService.getItems('15','locations/locationbycategoria').subscribe(
			result => {
				this.items = result.locations;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this._formGroup.controls; }
}