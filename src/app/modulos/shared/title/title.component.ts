import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
	selector: 'app-title',
	templateUrl: './title.component.html'
})
export class TitleComponent {
	title: string;

	constructor(private router: Router, private route: ActivatedRoute, private titleService: Title) {
		this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => {
			let currentRoute = this.route.root;
			let title = '';
			do {
				const childrenRoutes = currentRoute.children;
				currentRoute = null;
				childrenRoutes.forEach(r => {
					if (r.outlet === 'primary') {
						title = r.snapshot.data.title ? r.snapshot.data.title : r.snapshot.data.breadcrumb;
						currentRoute = r;
					}
				});
			} while (currentRoute);

			this.title = title;
			this.titleService.setTitle(this.title + ' • JB Nomina');
		});
	}
}
