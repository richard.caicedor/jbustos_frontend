import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

import { IPagerFilter, LabelPosition, PagerService } from '../../../../services/util';
import { SelectOptions } from '../../select';

@Component({
	selector: 'app-pager-select-column-filter',
	templateUrl: './pager-select-column-filter.component.html'
})
export class PagerSelectColumnFilterComponent implements OnInit, IPagerFilter {
	@Input() items: any[];
	@Input() valueField = 'id';
	@Input() displayField = 'name';
	@Input() name: string;
	@Input() label: string;
	@Input() labelPos: LabelPosition = 'left';
	@Input() default: any;
	@Input() searchable = true;
	@Input() selectOptions: SelectOptions;

	@ContentChild(TemplateRef, { static: true }) templateRef: TemplateRef<any>;

	selected: any;
	value: Subject<any> = new Subject();

	constructor(private pager: PagerService) {}

	ngOnInit() {
		this.pager.filterInitiated(this, !this.label);
	}

	valueReady(value) {
		if (value) this.selected = parseInt(value, null);
	}

	onChange(e) {
		if (!e) this.value.next(null);
		else this.value.next(e[this.valueField]);
	}

	select(e) {
		this.value.next(e);
		this.selected = parseInt(e, null);
	}
}
