import { ChangeDetectorRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Subject } from 'rxjs';
import { FunctionsService } from 'src/app/services/util';

export interface ILocalFilter {
	name: string;
	value: Subject<any>;
	valueReady(value): void;
}

export class LocalDt {
	/**
	 * Registros base
	 */
	private _rows: any[];
	public get rows(): any[] {
		return this._rows;
	}
	public set rows(v: any[]) {
		this._rows = v;
		this.filtered = v;

		this.filters.columns.forEach(f => {
			const value = this.filters.values[f.name];
			if (value) f.value.next(value);
		});
	}

	/**
	 * Registros filtrados
	 */
	private _filtered: any[];
	public get filtered(): any[] {
		return this._filtered;
	}
	public set filtered(v: any[]) {
		this._filtered = v;
		if (this.table) {
			this.table.rows = v;
			this.table.offset = 0;
			// this.cd.detectChanges();
		}
	}

	/**
	 * Numero de filas a mostrar por página
	 */
	private _size: number;
	public get size(): number {
		return this._size;
	}
	public set size(v: number) {
		this._size = v;
		this.table.offset = 0;
		this.table.limit = v;
		this.cd.detectChanges();
	}

	/**
	 * Mostrar loader de tabla
	 */
	private _loading = true;
	public get loading() {
		return this._loading;
	}
	public set loading(v: boolean) {
		this._loading = v;
		if (this.table) {
			this.table.loadingIndicator = v;
			setTimeout(() => {
				this.cd.detectChanges();
			}, 0);
		}
	}

	private cd: ChangeDetectorRef;
	private table: DatatableComponent;
	private fn: FunctionsService;
	private lastPage: number;

	private filters = {
		columns: [] as ILocalFilter[],
		values: {}
	};

	constructor() {}

	init(table: DatatableComponent, cd: ChangeDetectorRef, limit: number, fn: FunctionsService) {
		this.cd = cd;
		this.table = table;
		this.size = limit;
		this.loading = this._loading;
		this.fn = fn;
		this.table.page.subscribe(value => {
			this.lastPage = value.offset;
		});

		setTimeout(() => {
			if (this._filtered) {
				this.table.rows = this._filtered;
				this.table.offset = 0;
				this.cd.detectChanges();
			}

			if (this.lastPage != null) {
				this.table.offset = this.lastPage;
				this.cd.detectChanges();
			}

			this.filters.columns.forEach(f => {
				const value = this.filters.values[f.name];
				f.valueReady(value);
			});
		}, 0);
	}

	filterInitiated(filter: ILocalFilter, inColumn: boolean) {
		if (inColumn) this.table.headerHeight = 60;

		this.filters.columns.push(filter);
		filter.value.subscribe(value => {
			this.filters.values[filter.name] = this.fn.removeAccents(value);
			const regex = new RegExp(this.fn.removeAccents(this.filters.values[filter.name]).replaceAll(' ', '.*.'));

			this.filtered = this.rows.filter(item => {
				return Object.keys(this.filters.values).every(filterName => {
					return !!this.fn.removeAccents(item[filterName]).match(regex) || !this.filters.values[filterName];
				});
			});

			if (!value) delete this.filters.values[filter.name];
		});
	}
}
