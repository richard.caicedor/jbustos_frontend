import './extensions';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SuiDimmerModule } from 'ng2-semantic-ui';
import { MomentModule } from 'ngx-moment';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MarkTouchedOnBlurDirective } from 'src/app/directives/index.ts';

import { CheckboxComponent } from './checkbox/checkbox.component';
import { DatatableHeaderComponent } from './datatable-header/datatable-header.component';
import { DimmerComponent } from './dimmer/dimmer.component';
import { FormControlErrorComponent } from './form-control-error/form-control-error.component';
import { LoadingContentComponent } from './loading-content/loading-content.component';
import { LocalDatatableDirective, LocalFilterSizeComponent, LocalInputColumnFilterComponent } from './local-dt';
import {
    PagerAddComponent,
    PagerButtonComponent,
    PagerButtonDeleteComponent,
    PagerButtonEditComponent,
    PagerCheckboxFilterComponent,
    PagerDatatableDirective,
    PagerDateColumnFilterComponent,
    PagerFilterComponent,
    PagerInputColumnFilterComponent,
    PagerSelectColumnFilterComponent,
    PagerSizeComponent,
    PagerSizeFilterComponent,
} from './pager';
import { PagerRangeDateColumnFilterComponent } from './pager/pager-range-date-column-filter/pager-range-date-column-filter.component';
import { ReturnBackComponent } from './return-back/return-back.component';
import { SelectAsyncComponent, SelectComponent, SelectLabelDirective, SelectOptionDirective } from './select';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		NgxPermissionsModule,
		NgxDatatableModule,
		NgbModule,
		NgSelectModule,
		SuiDimmerModule,
		MomentModule
	],
	declarations: [
		FormControlErrorComponent,
		MarkTouchedOnBlurDirective,
		PagerAddComponent,
		PagerFilterComponent,
		PagerSizeComponent,
		PagerSelectColumnFilterComponent,
		PagerInputColumnFilterComponent,
		PagerDateColumnFilterComponent,
		PagerRangeDateColumnFilterComponent,
		PagerDatatableDirective,
		PagerButtonEditComponent,
		PagerButtonDeleteComponent,
		PagerButtonComponent,
		PagerSizeFilterComponent,
		PagerCheckboxFilterComponent,
		DatatableHeaderComponent,
		SelectAsyncComponent,
		SelectComponent,
		SelectLabelDirective,
		SelectOptionDirective,
		CheckboxComponent,
		DimmerComponent,
		LoadingContentComponent,
		ReturnBackComponent,
		LocalDatatableDirective,
		LocalFilterSizeComponent,
		LocalInputColumnFilterComponent
	],
	exports: [
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		NgxPermissionsModule,
		FormControlErrorComponent,
		MarkTouchedOnBlurDirective,
		PagerAddComponent,
		PagerFilterComponent,
		PagerSizeComponent,
		PagerSelectColumnFilterComponent,
		PagerInputColumnFilterComponent,
		PagerDateColumnFilterComponent,
		PagerRangeDateColumnFilterComponent,
		PagerDatatableDirective,
		PagerButtonEditComponent,
		PagerButtonDeleteComponent,
		PagerButtonComponent,
		PagerSizeFilterComponent,
		PagerCheckboxFilterComponent,
		NgxDatatableModule,
		DatatableHeaderComponent,
		SelectAsyncComponent,
		SelectComponent,
		SelectLabelDirective,
		SelectOptionDirective,
		NgbModule,
		CheckboxComponent,
		DimmerComponent,
		ReturnBackComponent,
		LocalDatatableDirective,
		LocalFilterSizeComponent,
		LocalInputColumnFilterComponent,
		MomentModule
	],
	providers: []
})
export class SharedModule {}
