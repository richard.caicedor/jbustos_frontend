import {Component, OnInit } from '@angular/core';
import {Estudio} from '../../to/estudio';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-estudio',
	templateUrl: '../estudios/estudios.component.html',
	providers: [Servicio]
}) 

export class EstudiosComponent implements OnInit{

	public titulo:string;
	public items:Estudio[];
	public _formGroup:FormGroup;
	public textbtn:string;
	public submitted:boolean;
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.titulo = "Administración de Estudios";
		this._formGroup = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			name: new FormControl('',[Validators.required]),
			representative: new FormControl('',[Validators.required]),
			contact_phone: new FormControl('',[Validators.required]), 
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue("");  
	} 

	onEditar(item:Estudio){
		this.textbtn ='Modificar';
		this._formGroup.controls['id'].setValue(item.id);
		this._formGroup.controls['name'].setValue(item.name);
		this._formGroup.controls['representative'].setValue(item.representative);
		this._formGroup.controls['contact_phone'].setValue(item.contact_phone);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true; 
		if(this._formGroup.get('id').value !== ''){
			if(!this._formGroup.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this._formGroup.value,'studios/editstudio').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{  
			if(!this._formGroup.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.addItem(this._formGroup.value,'studios/studio').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		} 
	}

	ngOnInit(){
		this.getListas();
	}

	onEliminar(id:string){
		if(confirm('¿Desea eliminar el registro?')){
			this._InformacionService.getItems(id,'studios/deletestudio').subscribe(
				result => { 
					$('.loaderBox').hide(); 
					this.getListas();
				},
				error => {
					console.log(<any>error);
				} 
			); 
		}
	}

	getListas(){
		this._InformacionService.getItems('','studios/studio').subscribe(
			result => {
				this.items = result;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this._formGroup.controls; }
}