import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ActionsService } from 'src/app/services/api';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

@Component({
	selector: 'app-actions-form',
	templateUrl: './actions-form.component.html',
	styleUrls: ['./actions-form.component.scss'],
	providers: [ReturnPagerService]
})
export class ActionsFormComponent implements OnInit {
	loading: boolean;
	form: FormGroup;

	constructor(
		fb: FormBuilder,
		private actionsService: ActionsService,
		private notify: NotifyMessageService,
		public returnPager: ReturnPagerService,
		activated: ActivatedRoute
	) {
		this.returnPager.path = ['/actions'];

		this.form = fb.group({
			id: [null],
			name: [null, [Validators.required, Validators.maxLength(50)]],
			description: [null, [Validators.required, Validators.maxLength(50)]]
		});

		activated.params.subscribe(params => {
			const id = +params.id;
			if (!id) return;

			this.loading = true;
			actionsService.get(id, {
				success: res => this.form.patchValue(res.action),
				always: () => (this.loading = false)
			});
		});
	}

	ngOnInit() {}

	save() {
		if (!this.form.validate()) return;

		this.loading = true;
		this.actionsService.save(this.form.value, {
			success: () => {
				this.notify.success('Exito', 'Acción guardada');
				this.returnPager.back();
			},
			always: () => (this.loading = false)
		});
	}
}
