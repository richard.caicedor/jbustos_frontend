import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';
import { ActionsService } from 'src/app/services/api';

import { SharedModule } from '../shared/shared.module';
import { ActionsComponent } from './actions.component';
import { ActionsFormComponent } from './form/actions-form.component';

export const ActionsRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				canActivate: [PermissionGuard],
				component: ActionsComponent,
				data: {
					title: 'Administración de Acciones',
					permission: 'actions_view'
				}
			},
			{
				path: 'add',
				component: ActionsFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Agregar Acción',
					permission: 'actions_create'
				}
			},
			{
				path: 'edit/:id',
				component: ActionsFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Editar Acción',
					permission: 'actions_edit'
				}
			}
		]
	}
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(ActionsRoutes), SharedModule],
	declarations: [ActionsComponent, ActionsFormComponent],
	providers: [ActionsService]
})
export class ActionsModule {}
