import { Component, OnInit } from '@angular/core';
import { ActionsService } from 'src/app/services/api';
import { MessageType, PagerService, SwalMessageService } from 'src/app/services/util';

@Component({
	selector: 'app-actions',
	templateUrl: './actions.component.html',
	styleUrls: ['./actions.component.scss'],
	providers: [PagerService]
})
export class ActionsComponent implements OnInit {
	constructor(private actionsService: ActionsService, private swal: SwalMessageService, public pager: PagerService) {}

	ngOnInit() {}

	/**
	 * Mostrar listado
	 */
	getData() {
		this.pager.loading = true;
		this.actionsService.index(this.pager.getParams(), {
			success: res => {
				this.pager.rows = res.actions;
				this.pager.count = res.count;
			},
			always: () => (this.pager.loading = false)
		});
	}

	/**
	 * Borrar
	 *
	 * @param row
	 */
	delete(row: any): void {
		this.swal.confirmationAjax('Eiminar Acción', 'Esta seguro que desea eliminar la acción?', {
			ok: {
				text: 'Eliminar',
				handler: () => {
					this.actionsService.delete(row.id, {
						success: () => {
							this.swal.success('Eliminado', 'Acción eliminada');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}
}
