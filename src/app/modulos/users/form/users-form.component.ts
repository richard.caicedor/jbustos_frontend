import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RolesService, SedesService, StudiosService, UsersService } from 'src/app/services/api';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

import { CustomValidators } from '../../shared';
import { SelectOptions } from '../../shared/select';

@Component({
	selector: 'app-users-form',
	templateUrl: './users-form.component.html',
	styleUrls: ['./users-form.component.scss'],
	providers: [ReturnPagerService, StudiosService, SedesService, RolesService]
})
export class UsersFormComponent implements OnInit {
	loading: boolean;
	form: FormGroup;
	id: number;

	list = {
		studios: new SelectOptions(this.studiosService.list),
		sedes: new SelectOptions(this.sedesService.list),
		roles: new SelectOptions(this.rolesService.list)
	};

	private initialRolesId = [];

	constructor(
		fb: FormBuilder,
		private usersService: UsersService,
		private studiosService: StudiosService,
		private sedesService: SedesService,
		private rolesService: RolesService,
		private notify: NotifyMessageService,
		public returnPager: ReturnPagerService,
		activated: ActivatedRoute
	) {
		this.returnPager.path = ['/users'];

		this.list.studios.fetch();
		this.list.sedes.fetch();
		this.list.roles.fetch();

		this.form = fb.group({
			id: [null],
			name: [null, [Validators.required, Validators.maxLength(255)]],
			nick: [null, [Validators.required, Validators.maxLength(255)]],
			email: [null, [Validators.required, Validators.email, Validators.maxLength(255)]],
			password: [null, [Validators.required, Validators.minLength(8)]],
			confirmation: [null],
			sede_id: [null],
			studio_id: [null],
			roles: [null, [Validators.required]]
		});

		this.form.controls.confirmation.setValidators([CustomValidators.equalField('password')]);

		this.form.customErrorMessages = {
			confirmation: { equalField: 'Debe confirmar la contraseña' }
		};

		activated.params.subscribe(params => {
			this.id = +params.id;
			if (!this.id) return;

			this.form.controls.password.setValidators([]);

			this.loading = true;
			usersService.get(this.id, {
				success: res => {
					this.form.patchValue(res.user);
					this.initialRolesId = res.user.roles;
				},
				always: () => (this.loading = false)
			});
		});
	}

	ngOnInit() {}

	save() {
		if (!this.form.validate()) return;

		const data = this.form.value;
		data.roles = this.initialRolesId.difference(data.roles);
		delete data.confirmation;

		this.loading = true;
		this.usersService.save(data, {
			success: () => {
				this.notify.success('Exito', 'Usuario guardardo');
				this.returnPager.back();
			},
			always: () => (this.loading = false)
		});
	}
}
