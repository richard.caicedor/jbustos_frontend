import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';
import { ActionsService } from 'src/app/services/api';

import { SharedModule } from '../shared/shared.module';
import { UsersFormComponent } from './form/users-form.component';
import { UsersComponent } from './users.component';

export const UsersRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				canActivate: [PermissionGuard],
				component: UsersComponent,
				data: {
					title: 'Administración de Usuarios',
					permission: 'users_view'
				}
			},
			{
				path: 'add',
				component: UsersFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Agregar Usuario',
					permission: 'users_create'
				}
			},
			{
				path: 'edit/:id',
				component: UsersFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Editar Usuario',
					permission: 'users_edit'
				}
			}
		]
	}
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(UsersRoutes), SharedModule],
	declarations: [UsersComponent, UsersFormComponent],
	providers: [ActionsService]
})
export class UsersModule {}
