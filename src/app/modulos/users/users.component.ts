import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/api';
import { MessageType, PagerService, SwalMessageService } from 'src/app/services/util';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss'],
	providers: [PagerService]
})
export class UsersComponent implements OnInit {
	constructor(private usersService: UsersService, private swal: SwalMessageService, public pager: PagerService) {}

	ngOnInit() {}

	/**
	 * Mostrar listado
	 */
	getData() {
		this.pager.loading = true;
		this.usersService.index(this.pager.getParams(), {
			success: res => {
				this.pager.rows = res.users;
				this.pager.count = res.count;
			},
			always: () => (this.pager.loading = false)
		});
	}

	/**
	 * Borrar
	 *
	 * @param row
	 */
	delete(row: any): void {
		this.swal.confirmationAjax('Eiminar Usuario', 'Esta seguro que desea eliminar el usuario?', {
			ok: {
				text: 'Eliminar',
				handler: () => {
					this.usersService.delete(row.id, {
						success: () => {
							this.swal.success('Eliminado', 'Usuario eliminado');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}
}
