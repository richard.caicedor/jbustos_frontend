import {Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid-community";
import {Servicio} from '../../services/servicios.service';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({  
    selector: 'app-ventas',
    templateUrl: './ventas.component.html',
    providers: [Servicio]
})

export class VentasComponent implements OnInit {
    
    public gridOptions:GridOptions; 
    public sedes:any[];
    public paginas:any[];
    public consultaForm:FormGroup;
    public submitted:boolean;
    public btnSubir:boolean; 
    public filesToUpload;
    public fileForm:FormGroup;

    constructor(public _servicio:Servicio,private notify: NotifyMessageService){
        this.gridOptions = <GridOptions>{
            //rowData: VentasComponent.Datos(),
            columnDefs: VentasComponent.Columnas(),
            context: {
                componentParent: this
            },
            components: {
                'FormatoCelda': this.FormatoCelda
            }
        };
        this.consultaForm  = this.createFormConsulta();
        this.fileForm  = this.createFormGroupFile();
        this.submitted = false; 
    }

    private createFormConsulta(){   
        return new FormGroup({
            sede_id: new FormControl(null,[Validators.required]),
            page_id: new FormControl(null,[Validators.required]),
            fecha_inicio: new FormControl(null,[Validators.required]),
            fecha_fin: new FormControl(null,[Validators.required]),
        }); 
    }

    createFormGroupFile(){
        return new FormGroup({
            file: new FormControl('',[Validators.required]),
            fecha_inicio: new FormControl('',[Validators.required]),
            fecha_fin: new FormControl('',[Validators.required]),
            page_id: new FormControl(null,[Validators.required])
        }); 
    }

    private static Columnas(){
        return [
            {headerName: 'Id', field: 'model_id', hide: "true"},
            {headerName: 'Identificación', field: 'identificacion', pinned: 'left', width: 250},
            {headerName: 'Nombre', field: 'nombre' , width: 250, pinned: 'left'},
            {headerName: 'Nick', field: 'nick' , width: 250},
            {headerName: 'Ventas', field: 'ventas', editable: true,  cellRenderer: 'FormatoCelda', cellStyle: { 'text-align': "right" }},
        ];
    }

    public ChangeCargarArchivo(fileInput:any){
        this.filesToUpload = <Array<File>>fileInput.target.files; 
    }

    private static Datos(){
        /*return [
            { model_id: 1 ,identificacion: '123456', nombre: 'Andrea Benites' },
        ];*/ 
    }


    public onConsulta(){
        this.submitted = true;
        if(!this.consultaForm.invalid){
            $('.loaderBox').show();
            this._servicio.addItem(this.consultaForm.value,'ventas/nicksventa').subscribe(
                result => { 
                    $('.loaderBox').hide();  
                    //console.log(result);
                    this.gridOptions.api.setRowData(result);
                    //this.getListas();
                },
                error => {
                    console.log(<any>error);
                } 
            );         
            console.log(this.consultaForm.value);
            $('#detalleConsulta').show(300);
        }
    }

    ngOnInit(){
        this.getListas();
    }

    private FormatoCelda(params:any) {
        var usdFormate = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 1
        }); 
        return usdFormate.format(params.value);
    }

    private OnGuardarDatos() {
      let rowData = [];
      this.gridOptions.api.forEachNode(node => rowData.push(node.data));
      rowData.push(this.consultaForm.value);
      $('.loaderBox').show();
      this._servicio.addItem(rowData,'ventas/savesnicksventa').subscribe(
        response => {
            if(response.status !== 'Ok'){
                    this.notify.error('Error', 'Error al guardar Ventas!');
            }else{ 
                this.notify.success('Éxito', 'Ventas Almacenadas Éxitosamente!');
            }
            $('.loaderBox').hide();
        }, 
        error => {
          console.log(<any>error);
        } 
      );
      console.log(rowData)
      return rowData; 
    }
 
    getListas(){
        this._servicio.getItems('0','ventas/basicInfo').subscribe(
            result => { 
                this.sedes = result.sedes;
                this.paginas = result.paginas;
                $('.loaderBox').hide();
            }, 
            error => {
                console.log(<any>error);
            }
        );
    }

    onEnviarArchivo(){  
        this.btnSubir = true; 
        if(!this.fileForm.invalid){
            console.log(this.filesToUpload);
            console.log(this.fileForm.value); 
            this.onResetForm();
            $('#btnCerrarModal').click();
            /*
            this._servicio.UploadFile('', this.filesToUpload, this.fileForm.value);
            $('#btnCerrarModal').click();
            $('#alertMessage').html('<b>Carga de Ventas Éxitosa!</b>');
            $('#alertMessage').show();
            setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
            */
        }
        
    }

    onResetForm(){
        this.fileForm.reset();
    }

    get f() { return this.consultaForm.controls; }
    get g() { return this.fileForm.controls; }
}