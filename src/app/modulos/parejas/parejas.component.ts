import {Component, OnInit } from '@angular/core';
import {Pareja} from '../../to/pareja';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-parejas',
	templateUrl: './parejas.component.html',
	providers: [Servicio]
})

export class ParejaComponent implements OnInit{  

	public titulo:string;
	public items:Pareja[];
	public _Form:FormGroup;
	public textbtn:string;
	public modelos:any[];
	submitted:boolean;
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.titulo = "Administración de Parejas";
		this._Form = this.createFormGroup();
		this.submitted = false;
	} 

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			model_id: new FormControl('',[Validators.required]),
			couple_id: new FormControl('',[Validators.required]),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this._Form.reset();
		this._Form.controls['id'].setValue(""); 
	} 

	onEditar(item:Pareja){
		this.textbtn ='Modificar';
		this._Form.controls['id'].setValue(item.id);
		this._Form.controls['model_id'].setValue(item.model_id);
		this._Form.controls['couple_id'].setValue(item.couple_id);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true;
		if(this._Form.get('id').value !== ''){
			if(!this._Form.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this._Form.value,'parejas/editparmodelo').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			if(!this._Form.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.addItem(this._Form.value,'parejas/parmodelos').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	onEliminar(id:string){
		if(confirm('¿Desea eliminar el registro?')){
			this._InformacionService .getItems(id,'parejas/deleteparmodelo').subscribe(
				result => { 
					$('.loaderBox').hide();  
					this.getListas(); 
				},
				error => {
					console.log(<any>error);
				}   
			); 
		}
	}
 
	getListas(){   
		this._InformacionService.getItems('','parejas/parmodelos').subscribe(
			result => {
				this.items = result.parejas;
				this.modelos = result.modelos;
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				})
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this._Form.controls; }
}