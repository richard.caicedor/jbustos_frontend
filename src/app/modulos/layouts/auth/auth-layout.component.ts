import { Component, OnInit } from '@angular/core';
import { NotifyMessageService } from 'src/app/services/util';

@Component({
	selector: 'app-auth-layout',
	templateUrl: './auth-layout.component.html',
	styleUrls: ['./auth-layout.component.css']
})
export class AuthLayoutComponent implements OnInit {
	constructor(public notify: NotifyMessageService) {}

	ngOnInit() {}
}
