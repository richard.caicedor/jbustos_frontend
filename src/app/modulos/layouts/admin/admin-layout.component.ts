import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsService } from 'ngx-permissions';
import { Subscription } from 'rxjs';
import { MenuService, UsersService } from 'src/app/services/api';
import { ErrorUtil, MessageType, NotifyMessageService, SessionService } from 'src/app/services/util';

import { CustomValidators } from '../../shared';

@Component({
	selector: 'app-admin-layout',
	templateUrl: './admin-layout.component.html',
	styleUrls: ['./admin-layout.component.css'],
	providers: [MenuService]
})
export class AdminLayoutComponent implements OnInit, OnDestroy {
	mainMenu: any[];
	title: string;

	changePassword = {
		form: null as FormGroup,
		loading: null as boolean
	};

	private subs = new Subscription();

	constructor(
		private userService: UsersService,
		public sessionService: SessionService,
		public notify: NotifyMessageService,
		private menuService: MenuService,
		private modalService: NgbModal,
		ngxPermisisonsService: NgxPermissionsService,
		activated: ActivatedRoute,
		fb: FormBuilder
	) {
		this.subs.add(
			ngxPermisisonsService.permissions$.subscribe(value => {
				if (Object.keys(value).length > 0) this.getMainMenu();
			})
		);

		this.subs.add(
			activated.data.subscribe(data => {
				this.title = data.title;
			})
		);

		this.changePassword.form = fb.group({
			current: [null, [Validators.required]],
			new: [null],
			confirmation: [null]
		});
		this.changePassword.form.controls.new.setValidators([Validators.required, Validators.minLength(8), CustomValidators.diffField('current')]);
		this.changePassword.form.controls.confirmation.setValidators([CustomValidators.equalField('new')]);
		this.changePassword.form.customErrorMessages = {
			new: { diffField: 'La nueva contraseña debe ser diferente a la actual' },
			confirmation: { equalField: 'Debe confirmar la contraseña' }
		};
	}

	ngOnInit() {
		$('.loaderBox').hide();

		if (!this.sessionService.user) {
			this.userService.current({
				success: res => this.sessionService.setUser(res.user, res.actions)
			});
		}
	}

	ngOnDestroy() {
		this.subs.unsubscribe();
	}

	logout() {
		this.userService.logout({});
		this.sessionService.logout();
	}

	getMainMenu() {
		this.menuService.tree({
			success: res => {
				const handleMenu = (nodes: any[]) => {
					const temp = nodes.filter(n => {
						if (n.children) {
							n.children = handleMenu(n.children);

							if (!n.children.length) return false;
						}

						return n.action_name ? this.sessionService.permissions.includes(n.action_name) : true;
					});

					return temp;
				};

				this.mainMenu = handleMenu(res.menu);
			}
		});
	}

	open(content) {
		this.changePassword.form.reset();
		this.modalService.open(content);
	}

	savePassword(modal) {
		if (!this.changePassword.form.validate()) return;

		this.changePassword.loading = true;
		this.userService.changePassword(this.changePassword.form.value, {
			success: () => {
				this.notify.success('Exito', 'Contraseña cambiada');
				modal.close();
			},
			errorMsgType: MessageType.None,
			error: (res, err: ErrorUtil) => {
				if (res && res.type === 'invalid_current_password') {
					this.notify.error('Error', 'Contraseña actual incorrecta');
					this.changePassword.form.controls.current.setValue(null);
				} else {
					err.showNotifyError();
				}
			},
			always: () => (this.changePassword.loading = false)
		});
	}
}
