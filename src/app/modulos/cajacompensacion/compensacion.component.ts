import {Component, OnInit } from '@angular/core';
import {Informacion} from '../../to/informacion';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-arl',
	templateUrl: './compensacion.component.html', 
	providers: [Servicio]
}) 

export class CompensacionComponent implements OnInit{

	public items:Informacion[];
	public _formGroup:FormGroup;
	public textbtn:string;
	public modulo:string;
	public submitted:boolean;
	public lblDependencia:string;
	public selectItems:any[];
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.modulo = " Caja Compensación";
		this._formGroup = this.createFormGroup();
		this.submitted = false;
		this.lblDependencia= null;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
			category_id: new FormControl(''),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this._formGroup.reset();
		this._formGroup.controls['id'].setValue(""); 
	} 

	onEditar(item:Informacion){
		this.textbtn ='Modificar';
		this._formGroup.controls['id'].setValue(item.id);
		this._formGroup.controls['description'].setValue(item.description);
		this._formGroup.controls['category_id'].setValue(29);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}
 
	onSaveForm(){
		this.submitted = true; 
		if(this._formGroup.get('id').value !== ''){
			if(!this._formGroup.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this._formGroup.value,'informacion/editbasicinfo').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			if(!this._formGroup.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._formGroup.controls['category_id'].setValue(29); 
				this._InformacionService.addItem(this._formGroup.value,'informacion/basicinfo').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._InformacionService.getItems('29','informacion/basicinfo').subscribe(
			result => {
				this.items = result;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this._formGroup.controls; }
}