import {Component, OnInit } from '@angular/core';
import {Pagina} from '../../to/pagina';
import {Sede} from '../../to/Sede';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-sedes',
	templateUrl: './sedes.component.html',
	providers: [Servicio]
})

export class SedesComponent implements OnInit{

	public sedes:Sede[]; 
	sedeForm:FormGroup;
	public textbtn:string;
	public bancos:any[];
	public pagos:any[];
	public monedas:any[];
	submitted:boolean;
	public campoEmail:boolean;
	public campoNumero:boolean;
 
	constructor(public _SedeService:Servicio){
		this.textbtn ='Guardar';
		this.sedeForm = this.createFormGroup();
		this.submitted = false;
		this.campoEmail = false;
		this.campoNumero = false;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			name: new FormControl('',[Validators.required]),
			representative: new FormControl('',[Validators.required]),
			phone: new FormControl('',[Validators.required]),
			email: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			type: new FormControl('',[Validators.required]),
			address: new FormControl('',[Validators.required]),
			banco_id: new FormControl('',[Validators.required]),
			cuenta: new FormControl('',[Validators.required]),
			nit: new FormControl('',[Validators.required]),
			titular: new FormControl('',[Validators.required]),
			tipo_pago: new FormControl('',[Validators.required]),
			moneda: new FormControl('',[Validators.required]),
			numero: new FormControl(''),
			email_pago: new FormControl(''),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this.sedeForm.reset();
		this.sedeForm.controls['id'].setValue("");
	} 

	onEditar(sede:Sede){
		this.textbtn ='Modificar';
		this.sedeForm.controls['id'].setValue(sede.id);
		this.sedeForm.controls['name'].setValue(sede.name);
		this.sedeForm.controls['representative'].setValue(sede.representative);
		this.sedeForm.controls['phone'].setValue(sede.phone);
		this.sedeForm.controls['email'].setValue(sede.email);
		this.sedeForm.controls['type'].setValue(sede.type);
		this.sedeForm.controls['address'].setValue(sede.address);
		this.sedeForm.controls['banco_id'].setValue(sede.banco_id);
		this.sedeForm.controls['cuenta'].setValue(sede.cuenta);
		this.sedeForm.controls['nit'].setValue(sede.nit);
		this.sedeForm.controls['titular'].setValue(sede.titular);
		this.sedeForm.controls['tipo_pago'].setValue(sede.tipo_pago);
		this.sedeForm.controls['moneda'].setValue(sede.moneda);
		this.sedeForm.controls['numero'].setValue(sede.numero);
		this.sedeForm.controls['email_pago'].setValue(sede.email_pago);
		this.onChangeCampo(sede.tipo_pago)
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onChangeCampo(e){
		this.campoEmail = false;
		this.campoNumero = false;
		if(e == 'Paxum' || e == 'Paypal' ){
			this.campoEmail = true;
		}else if (e == 'Epayservices' ){
			this.campoNumero = true;
		}
	}

	onSaveForm(){
		this.submitted = true;
		if(this.sedeForm.get('id').value !== ''){
			if(!this.sedeForm.invalid){
				this.textbtn ='Procesando...'; 
				$('#btnGuardar').attr('disabled','true');
				this._SedeService.editItem(this.sedeForm.value,'sedes/editsede').subscribe( 
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );  
			}
		} else{
			if(!this.sedeForm.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				console.log(this.sedeForm.value)
				this._SedeService.addItem(this.sedeForm.value,'sedes/sede').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._SedeService.getItems('0','sedes/sede').subscribe(
			result => {
				this.sedes = result.sedes;
				this.bancos = result.bancos;
				this.pagos = [
					{id: 'Banco', description:'Banco'},
					{id: 'Paxum', description:'Paxum'},
					{id: 'Paypal', description:'Paypal'},
					{id: 'Epayservices', description:'Epayservices'},
					{id: 'Efectivo', description:'Efectivo'},
				];
				this.monedas = [
					{id: 'Pesos', description:'Pesos'},
					{id: 'Dolares', description:'Dolares'},
				];
				$('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this.sedeForm.controls; }
}