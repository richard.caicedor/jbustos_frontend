import {Component, OnInit } from '@angular/core';
import {Pagina} from '../../to/pagina';
import {Sede} from '../../to/Sede';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-sedes',
	templateUrl: './sededocumento.component.html',
	providers: [Servicio]
})

export class SedeDocumentoComponent implements OnInit{

	public titulo:string;
	public sedes:Sede[]; 
	public sedeForm:FormGroup;
	public textbtn:string;
	public submitted:boolean;
	public filesToUpload;
 
	constructor(public _SedeService:Servicio){
		this.textbtn ='Guardar';
		this.titulo = "Administración de documentos por sedes";
		this.sedeForm = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			sede_id: new FormControl('',[Validators.required]),
			file: new FormControl('',[Validators.required]),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this.sedeForm.reset();
	} 

	public ChangeCargarArchivo(fileInput:any){
        this.filesToUpload = <Array<File>>fileInput.target.files; 
    }

	onEditar(sede:Sede){
		this.sedeForm.controls['sede_id'].setValue(sede.id);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true;
		var info = [{
			'sede_id': this.sedeForm.get('sede_id').value,
			'file' : this.filesToUpload
		}]
        console.log(info); 
        


		/*
		if(!this.sedeForm.invalid){ 
			this.textbtn ='Procesando...';
			$('#btnGuardar').attr('disabled','true');
				this._SedeService.addItem(this.sedeForm.value,'sede').subscribe(
			    response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			    }, 
			    error => {
			        console.log(<any>error);
		        } 
		    );
		}
		*/
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._SedeService.getItems('0','sede').subscribe(
			result => {
				this.sedes = result; 
				$('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this.sedeForm.controls; }
}