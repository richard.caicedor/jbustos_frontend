import { Component, OnInit } from '@angular/core';
import { RolesService } from 'src/app/services/api';

import { MessageType, PagerService, SwalMessageService } from '../../services/util';

@Component({
	selector: 'app-roles',
	templateUrl: './roles.component.html',
	providers: [PagerService]
})
export class RolesComponent implements OnInit {
	constructor(private rolesService: RolesService, private swal: SwalMessageService, public pager: PagerService) {}

	ngOnInit() {}

	/**
	 * Mostrar listado
	 */
	getData() {
		this.pager.loading = true;
		this.rolesService.index(this.pager.getParams(), {
			success: res => {
				this.pager.rows = res.roles;
				this.pager.count = res.count;
			},
			always: () => (this.pager.loading = false)
		});
	}

	/**
	 * Borrar
	 *
	 * @param row
	 */
	delete(row: any): void {
		this.swal.confirmationAjax('Eiminar Rol', 'Esta seguro que desea eliminar el rol?', {
			ok: {
				text: 'Eliminar',
				handler: () => {
					this.rolesService.delete(row.id, {
						success: () => {
							this.swal.success('Eliminado', 'Rol eliminado');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}
}
