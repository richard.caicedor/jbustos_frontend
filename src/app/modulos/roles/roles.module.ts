import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';
import { RolesService } from 'src/app/services/api';

import { SharedModule } from '../shared/shared.module';
import { RolesActionsComponent } from './actions/roles-actions.component';
import { RolesFormComponent } from './form/roles-form.component';
import { RolesComponent } from './roles.component';

export const RoleRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				canActivate: [PermissionGuard],
				component: RolesComponent,
				data: {
					title: 'Administración de Roles',
					permission: 'roles_view'
				}
			},
			{
				path: 'add',
				component: RolesFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Agregar Rol',
					permission: 'roles_create'
				}
			},
			{
				path: 'edit/:id',
				component: RolesFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Editar Rol',
					permission: 'roles_edit'
				}
			},
			{
				path: 'actions/:id',
				component: RolesActionsComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Acciones de Rol',
					permission: 'roles_actions'
				}
			}
		]
	}
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(RoleRoutes), SharedModule],
	declarations: [RolesComponent, RolesFormComponent, RolesActionsComponent],
	providers: [RolesService]
})
export class RoleModule {}
