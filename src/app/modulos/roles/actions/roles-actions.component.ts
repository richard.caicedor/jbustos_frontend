import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RolesService } from 'src/app/services/api';
import { FunctionsService, NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

import { LocalDt } from '../../shared/local-dt';

@Component({
	selector: 'app-roles-actions',
	templateUrl: './roles-actions.component.html',
	styleUrls: ['./roles-actions.component.scss'],
	providers: [ReturnPagerService]
})
export class RolesActionsComponent implements OnInit {
	loading: boolean;
	initial: any[];
	role: any;
	localDt = new LocalDt();

	private id = 0;

	constructor(
		private rolesService: RolesService,
		public returnPager: ReturnPagerService,
		private notify: NotifyMessageService,
		private activatedRoute: ActivatedRoute,
		private fn: FunctionsService
	) {
		this.returnPager.path = ['/roles'];
	}

	ngOnInit() {
		this.activatedRoute.params.subscribe(params => {
			this.id = params.id;
			this.loading = true;
			this.localDt.loading = true;

			this.rolesService.get(this.id, { success: res => (this.role = res.rol) });
			this.rolesService.actions(this.id, {
				success: res => {
					res.actions.map(item => {
						item.checked = item.checked === 1;
						return item;
					});
					this.localDt.rows = res.actions;
					this.setInitialArray();
					this.loading = false;
					this.localDt.loading = false;
				}
			});
		});
	}

	setInitialArray() {
		this.initial = this.fn.arrayCheck(this.localDt.rows, 'id', 'checked');
	}

	checkAll(checked: boolean) {
		this.localDt.filtered.forEach(item => {
			item.checked = checked;
		});
	}

	save(): void {
		this.loading = true;
		const final = this.fn.arrayCheck(this.localDt.rows, 'id', 'checked');

		this.rolesService.saveActions(this.id, this.fn.arrayDifference(this.initial, final), {
			success: () => {
				this.notify.success('Exito', 'Acciones de rol guardadas');
				this.returnPager.back();
			},
			always: () => (this.loading = false)
		});
	}
}
