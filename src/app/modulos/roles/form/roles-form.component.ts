import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RolesService } from 'src/app/services/api';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

@Component({
	selector: 'app-roles-form',
	templateUrl: './roles-form.component.html',
	styleUrls: ['./roles-form.component.scss'],
	providers: [ReturnPagerService]
})
export class RolesFormComponent implements OnInit {
	loading: boolean;
	form: FormGroup;

	constructor(
		fb: FormBuilder,
		private rolesService: RolesService,
		private notify: NotifyMessageService,
		public returnPager: ReturnPagerService,
		activated: ActivatedRoute
	) {
		this.returnPager.path = ['/roles'];

		this.form = fb.group({
			id: [null],
			name: [null, [Validators.required, Validators.maxLength(50)]]
		});

		activated.params.subscribe(params => {
			const id = +params.id;
			if (!id) return;

			this.loading = true;
			rolesService.get(id, {
				success: res => this.form.patchValue(res.rol),
				always: () => (this.loading = false)
			});
		});
	}

	ngOnInit() {}

	save() {
		if (!this.form.validate()) return;

		this.loading = true;
		this.rolesService.save(this.form.value, {
			success: () => {
				this.notify.success('Exito', 'Rol guardado');
				this.returnPager.back();
			},
			always: () => (this.loading = false)
		});
	}
}
