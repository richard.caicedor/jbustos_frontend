import {Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid-community";
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
//import {ClickableParentComponent} from "./acciones.component";
import * as $ from "jquery";

@Component({
	selector: 'app-recibos',
	templateUrl: './recibos.component.html',
	providers: [Servicio]
})

export class RecibosComponent implements OnInit{
    
    public titulo:string;
    public sedes:any[];
    public consultaForm:FormGroup;
    public submitted:boolean;
    public btnSubir:boolean; 
    public modelos:any[];
    public modelos_nomina:any[];

    constructor(public _servicio:Servicio){
        this.titulo = 'Recibos de Nómina';
        this.consultaForm  = this.createFormConsulta();
        this.submitted = false; 
    }

    private createFormConsulta(){   
        return new FormGroup({
            sedes_id: new FormControl(null,[Validators.required]),
            modelo_id: new FormControl(null,[Validators.required]),
            fecha_fin: new FormControl(null,[Validators.required]),
        }); 
    }

    public onDescargarRecibo(item){
    	console.log(item)
    }

    public onDescargarAll(){
    	console.log(this.consultaForm.value);
    }

    public onConsulta(){
        this.submitted = true;
        if(!this.consultaForm.invalid){
            console.log(this.consultaForm.value);
            this.modelos_nomina = [{model_id: 1 ,identification: '123456', name: 'Andrea Benites', email: 'ra@gmail.com', cellphone:'3125678945', admission_date:'01/01/2020', ventas: 0 }];
            $('#detalleConsulta').show(300);
        }
    }

    ngOnInit(){
        this.getListas();
    }

 
    getListas(){
        this._servicio.getItems('0','sede').subscribe(
            result => { 
                this.sedes = result;
                this.modelos = result.modelos;
                this.modelos = [{ id: 1, name:'Richard Caicedo'}]

                $('.loaderBox').hide();
            }, 
            error => {
                console.log(<any>error);
            }
        );
    }

    onResetForm(){
        //this.fileForm.reset();
    }

    get f() { return this.consultaForm.controls; }
}