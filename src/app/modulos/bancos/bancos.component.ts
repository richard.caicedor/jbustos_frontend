import {Component, OnInit } from '@angular/core';
import {Informacion} from '../../to/informacion';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-bancos',
	templateUrl: './bancos.component.html',
	providers: [Servicio]
}) 

export class BancosComponent implements OnInit{

	public items:Informacion[];
	public bancosForm:FormGroup;
	public textbtn:string;
	submitted:boolean;
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.bancosForm = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
			category_id: new FormControl(''),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this.bancosForm.reset();
		this.bancosForm.controls['id'].setValue(""); 
	} 

	onEditar(item:Informacion){
		this.textbtn ='Modificar';
		this.bancosForm.controls['id'].setValue(item.id);
		this.bancosForm.controls['description'].setValue(item.description);
		this.bancosForm.controls['category_id'].setValue(26);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true;
		if(this.bancosForm.get('id').value !== ''){
			if(!this.bancosForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this.bancosForm.value,'informacion/editbasicinfo').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			if(!this.bancosForm.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','x');
				this.bancosForm.controls['category_id'].setValue(26); 
				this._InformacionService.addItem(this.bancosForm.value,'informacion/basicinfo').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._InformacionService.getItems('26','informacion/basicinfo').subscribe(
			result => {
				this.items = result;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this.bancosForm.controls; }
} 