import {Component, OnInit } from '@angular/core';
import {Servicio} from '../../services/servicios.service';
import { Regla } from '../../to/regla';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";
  
@Component({
	selector: 'app-reglas',
	templateUrl: './reglas.component.html',
	providers: [Servicio]
})

export class ReglasComponent implements OnInit{

	public _form:FormGroup;
	public textbtn:string;
	public submitted:boolean; 
	public reglas:Regla[];
	public modelos:any[];
	public paginas:any[];
	public sedes:any[];    

	constructor(public _service:Servicio){
		this.textbtn ='Guardar';
		this._form  = this.createFormGroup();
		this.submitted = false; 
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			init_date: new FormControl('',[Validators.required]),
			minimun: new FormControl(''),
			maximum: new FormControl(''),
			percentage: new FormControl('',[Validators.required]),
			trm: new FormControl(''),
			page_id: new FormControl(''),
			model_id: new FormControl(''),
			sede_id: new FormControl('')
		});  
	}

	onResetForm(){
		this.textbtn ='Guardar';
		this._form.reset();
		this._form.controls['id'].setValue("");
	}

	onEditar(regla:Regla){
		this.textbtn ='Modificar';
		this._form.controls['id'].setValue(regla.id);
		this._form.controls['init_date'].setValue(regla.init_date);
		this._form.controls['minimun'].setValue(regla.minimun);
		this._form.controls['maximum'].setValue(regla.maximum);
		this._form.controls['percentage'].setValue(regla.percentage);
		this._form.controls['trm'].setValue(regla.trm);
		this._form.controls['page_id'].setValue(regla.page_id);
		this._form.controls['model_id'].setValue(regla.model_id);
		this._form.controls['sede_id'].setValue(regla.sede_id);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}
	
	onSaveForm(){
		this.submitted = true;
		if(this._form.get('id').value !== ''){
			if(!this._form .invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.editItem(this._form.value,'reglas/facturationruleEdit').subscribe(
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}
		} else{
			if(!this._form.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.addItem(this._form.value,'reglas/facturationrule').subscribe(
			      response => { 
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      },   
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._service.getItems('0','reglas/facturationrule').subscribe(
			result => { 
				this.paginas = result.paginas;
				this.sedes = result.sedes;
				this.modelos = result.modelos;
				this.reglas = result.reglas;
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				})
				$('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 

		
	}

	get f() { return this._form.controls; }
}