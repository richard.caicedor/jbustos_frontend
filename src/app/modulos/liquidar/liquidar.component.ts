import {Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid-community";
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';
import * as $ from "jquery";

@Component({
	selector: 'app-recibos',
	templateUrl: './liquidar.component.html',
	providers: [Servicio]
})

export class LiquidarComponent implements OnInit{
    
    public titulo:string;
    public sedes:any[];
    public consultaForm:FormGroup;
    public submitted:boolean;
    public btnSubir:boolean; 
    public modelos:any[];

    constructor(public _servicio:Servicio,private notify: NotifyMessageService){
        this.titulo = 'Proceso de Liquidación';
        this.consultaForm  = this.createFormConsulta();
        this.submitted = false; 
    }

    private createFormConsulta(){   
        return new FormGroup({
            sedes_id: new FormControl(null,[Validators.required]),
            modelo_id: new FormControl(null),
            fecha_fin: new FormControl(null,[Validators.required]),
        }); 
    }

    public onConsulta(){
        
        this.submitted = true;
        if(!this.consultaForm.invalid){
            $('.loaderBox').show();
            this._servicio.addItem(this.consultaForm.value,'ventas/liquidar').subscribe(
                result => { 
                    if(result.status !== 'Ok'){
                        this.notify.error('Error', 'Proceso de Liquidación con Errores!');
                    }else{ 
                        this.notify.success('Éxito', 'Proceso de Liquidación Éxitoso!');
                    }
                    $('.loaderBox').hide();
                    this.submitted = false;
                }, 
                error => {
                    console.log(<any>error);
                }
            );
            console.log(this.consultaForm.value);
        }
    }

    ngOnInit(){
        this.getListas();
    }

 
    getListas(){
        this._servicio.getItems('0','ventas/basicinfoliquidar').subscribe(
            result => { 
                this.sedes = result.sedes;
                this.modelos = result.modelos;

                $('.loaderBox').hide();
            }, 
            error => {
                console.log(<any>error);
            }
        );
    }

    onResetForm(){
        //this.fileForm.reset();
    }

    get f() { return this.consultaForm.controls; }
}