import {Component, OnInit } from '@angular/core';
import {Informacion} from '../../to/informacion';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-eps',
	templateUrl: './eps.component.html',
	providers: [Servicio]
})

export class EpsComponent implements OnInit{  

	public items:Informacion[];
	public epsForm:FormGroup;
	public textbtn:string;
	submitted:boolean;
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.epsForm = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
			category_id: new FormControl(''),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this.epsForm.reset();
		this.epsForm.controls['id'].setValue(""); 
	}  

	onEditar(item:Informacion){
		this.textbtn ='Modificar';
		this.epsForm.controls['id'].setValue(item.id);
		this.epsForm.controls['description'].setValue(item.description);
		this.epsForm.controls['category_id'].setValue(23);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true;
		if(this.epsForm.get('id').value !== ''){
			if(!this.epsForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this.epsForm.value,'informacion/editbasicinfo').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			if(!this.epsForm.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this.epsForm.controls['category_id'].setValue(23); 
				this._InformacionService.addItem(this.epsForm.value,'informacion/basicinfo').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}
 
	getListas(){
		this._InformacionService.getItems('23','informacion/basicinfo').subscribe(
			result => {
				this.items = result;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this.epsForm.controls; }
}