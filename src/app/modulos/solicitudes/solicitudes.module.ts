import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';
import { SolicitudesService } from 'src/app/services/api';

import { SharedModule } from '../shared/shared.module';
import { SolicitudesFormComponent } from './form/solicitudes-form.component';
import { SolicitudPagesComponent } from './pages/solicitud-pages.component';
import { SolicitudComponent } from './solicitudes.component';

export const SolicitudRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				canActivate: [PermissionGuard],
				component: SolicitudComponent,
				data: {
					title: 'Administración de Solicitudes',
					permission: 'request_view'
				}
			},
			{
				path: 'add',
				component: SolicitudesFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Agregar Solicitud',
					permission: 'request_create'
				}
			},
			{
				path: 'edit/:id',
				component: SolicitudesFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Editar Rol',
					permission: 'request_edit'
				}
			},
		]
	}
];


@NgModule({
	imports: [CommonModule, RouterModule.forChild(SolicitudRoutes), SharedModule],
	declarations: [SolicitudComponent, SolicitudesFormComponent, SolicitudPagesComponent],
	providers: [SolicitudesService]
})
export class SolicitudModule {}
