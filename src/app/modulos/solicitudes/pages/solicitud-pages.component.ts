import { Component, OnInit } from '@angular/core';
import { SolicitudesService } from 'src/app/services/api';
import { FunctionsService } from 'src/app/services/util';

import { LocalDt } from '../../shared/local-dt';

@Component({
	selector: 'app-solicitud-pages',
	templateUrl: './solicitud-pages.component.html',
	styleUrls: ['./solicitud-pages.component.scss']
})
export class SolicitudPagesComponent implements OnInit {
	loading: boolean;
	initial: any[];
	localDt = new LocalDt();

	constructor(private solicitudesService: SolicitudesService, private fn: FunctionsService) {}

	ngOnInit() {
		this.loading = true;
		this.localDt.loading = true;
		this.solicitudesService.pages({
			success: res => {
				res.pages.map(item => {
					item.checked = item.checked === 1;
					return item;
				});
				this.localDt.rows = res.pages;
				this.setInitialArray();
				this.loading = false;
				this.localDt.loading = false;
			}
		});
	}

	setInitialArray() {
		this.initial = this.fn.arrayCheck(this.localDt.rows, 'id', 'checked');
	}

	checkAll(checked: boolean) {
		this.localDt.filtered.forEach(item => {
			item.checked = checked;
		});
  }
  
  save(){}
}
