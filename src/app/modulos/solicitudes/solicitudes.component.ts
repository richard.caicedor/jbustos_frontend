import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SolicitudesService } from 'src/app/services/api';

import { MessageType, NotifyMessageService, PagerService, SessionService, SwalMessageService } from '../../services/util';

@Component({
	selector: 'app-solicitudes',
	templateUrl: './solicitudes.component.html',
	providers: [PagerService]
})
export class SolicitudComponent implements OnInit {
	comments = [];
	status = [{id: 8, name: "Actualizado"}, {id: 9, name: "Solicitud de cambios"}];
	selected: any;
	loading: boolean;
	commentDisabled: boolean;
	form: FormGroup;

	constructor(
		private solicitudesService: SolicitudesService,
		private swal: SwalMessageService,
		private notify: NotifyMessageService,
		public pager: PagerService,
		private modalService: NgbModal,
		fb: FormBuilder,
		private sessionService: SessionService
	) {
		this.form = fb.group({
			comment: [null, [Validators.required]],
			status_id_comment: [null, [Validators.required]]
		});
	}

	ngOnInit() {}

	/**
	 * Mostrar listado
	 */
	getData() {
		this.pager.loading = true;
		this.solicitudesService.index(this.pager.getParams(), {
			success: res => {
				this.pager.rows = res.solicitudes;
				this.pager.count = res.count;
			},
			always: () => (this.pager.loading = false)
		});
	}

	/**
	 * Borrar
	 *
	 * @param row
	 */
	delete(row: any): void {
		this.swal.confirmationAjax('Eiminar Rol', 'Esta seguro que desea eliminar el rol?', {
			ok: {
				text: 'Eliminar',
				handler: () => {
					this.solicitudesService.delete(row.id, {
						success: () => {
							this.swal.success('Eliminado', 'Rol eliminado');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}

	approve(row: any): void {
		this.swal.confirmationAjax('Aprobar Solicitud', 'Esta seguro que desea aprobar la solicitud?', {
			ok: {
				text: 'Aprobar',
				class: 'success',
				handler: () => {
					this.solicitudesService.approve(row.id, {
						success: () => {
							this.swal.success('Aprobada', 'Solicitud Aprobada');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}

	deny(row: any): void {
		this.swal.confirmationAjax('Rechazar Solicitud', 'Esta seguro que desea Rechazar la solicitud?', {
			ok: {
				text: 'Rechazar',
				handler: () => {
					this.solicitudesService.deny(row.id, {
						success: () => {
							this.swal.success('Rechazada', 'Solicitud Rechazada');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}

	open(row: any, content) {
		this.modalService.open(content, { size: 'xl', scrollable: true });
		this.form.reset();
		this.loading = true;
		this.commentDisabled = true;
		this.comments = [];
		this.selected = row;
		this.solicitudesService.comments(row.id, {
			success: res => {
				this.comments = res.comments;
			},
			always: () => {
				this.loading = false;
				this.commentDisabled = (row.status_id === 10 || row.status_id === 11);
			}
		});
	}

	saveComment() {
		if (!this.form.validate()) return;

		const comment = this.form.controls.comment.value;
		const status_id_comment = this.form.controls.status_id_comment.value;

		this.loading = true;
		this.commentDisabled = true;
		this.solicitudesService.saveComment(
			{ id: this.selected.id, comment, status_id_comment },
			{
				success: res => {
					const estado = this.status.find(element => element.id === status_id_comment);
					console.log(estado.name);
					this.notify.success('Exito', 'Comentario guardado');
					this.comments.splice(0, 0, {
						id: res.id,
						comment: comment,
						status: estado.name,
						user_name: this.sessionService.user.name,
						created_at: res.created_at
					});
					this.selected.status = estado.name;
					this.form.reset();
				},
				always: () => {
					this.loading = false;
					this.commentDisabled = false;
					this.getData();
				}
			}
		);
	}
}
