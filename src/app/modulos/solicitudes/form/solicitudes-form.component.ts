import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModelService, PagesService, SolicitudesService } from 'src/app/services/api';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

import { SelectOptions } from '../../shared/select';

@Component({
	selector: 'app-solicitudes-form',
	templateUrl: './solicitudes-form.component.html',
	styleUrls: ['./solicitudes-form.component.scss'],
	providers: [ReturnPagerService, PagesService, ModelService]
})
export class SolicitudesFormComponent implements OnInit {
	loading: boolean;
	form: FormGroup;
	id: number;
	btnGuardarDisabled: boolean;

	list = {
		pages: new SelectOptions(this.pagesService.list)
	};

	constructor(
		fb: FormBuilder,
		private solicitudesService: SolicitudesService,
		private notify: NotifyMessageService,
		private pagesService: PagesService,
		public modelService: ModelService,
		public returnPager: ReturnPagerService,
		private modalService: NgbModal,
		activated: ActivatedRoute
	) {
		this.list.pages.fetch();

		this.returnPager.path = ['/solicitudes'];

		this.form = fb.group({
			id: [null],
			model_id: [null, [Validators.required]],
			pages: [null, [Validators.required]],
			urlDropBox: [null],
			create_profile: [null],
			old_profile: [null],
			nicks_old_profile: [null],
			creator_old_profile: [null],
			other: [null],
			specifications: [null],
			social_media: [null],
			social_media_profile: [null],
			about_me: [null],
			tip_menu: [null],
			rules: [null],
			comments: [null],
			nicks_suggest: [null],
			page_id: [{ value: null, disabled: true }],
			model_disabled: [{ value: null, disabled: true }],
		});

		activated.params.subscribe(params => {
			this.id = +params.id;
			if (!this.id) return;

			this.form.controls.pages.setValidators([]);
			this.form.controls.model_id.setValidators([]);

			this.loading = true;
			this.btnGuardarDisabled = true;

			solicitudesService.get(this.id, {
				success: res => {
					this.btnGuardarDisabled = res.request.status_id === 10 || res.request.status_id === 11;
					this.form.patchValue(res.request);
					this.form.controls.model_disabled.setValue(this.form.controls.model_id.value);
				},
				always: () => (this.loading = false)
			});
		});
	}

	ngOnInit() {}

	save() {
		if (!this.form.validate()) return;

		const data = this.form.value;
		const temp = data.model_id.split('-');

		data.model_id = temp[0];
		data.type = temp[1];

		this.loading = true;
		this.btnGuardarDisabled = true;
		this.solicitudesService.save(data, {
			success: () => {
				this.notify.success('Exito', 'Solicitud guardado');
				this.returnPager.back();
			},
			always: () => {
				this.loading = false;
				this.btnGuardarDisabled = false;
			}
		});
	}

	open(content) {
		this.modalService.open(content, { size: 'xl' });
	}

	creatorOldChanged() {
		this.form.controls.other.setValidators([]);

		if (this.form.controls.creator_old_profile.value === 'o') {
			this.form.controls.other.setValidators([Validators.required]);
		}
	}
}
