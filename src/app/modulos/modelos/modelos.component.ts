import {Component, OnInit } from '@angular/core';
import { Sede } from '../../to/sede';
import { Modelo } from '../../to/modelo';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

const formData = new FormData()

@Component({
	selector: 'app-modelos',
	templateUrl: './modelos.component.html',
	providers: [Servicio]
})

export class ModelosComponent implements OnInit{

	public modelos:any[]; 
	public sedes:Sede[];
	public modeloForm:FormGroup;
	public fileForm:FormGroup;
	public textbtn:string;
	public eps:any[];
	public arl:any[];
	public fp:any[];
	public cdf:any[];
	public bancos:any[];
	public paises:any[];
	public ciudades:any[];
	public departamentos:any[];
	public ciudFilter:any[];
	public departFilter:any[];
	public nicks:any[];
	public paginas:any[];
	public modeloTable:any[];
	public submitted:boolean;
	public btnSubir:boolean; 
	public filesToUpload;
	public consultaForm:FormGroup;

	constructor(public _modeloService:Servicio){
		this.textbtn ='Guardar';
		this.modeloForm  = this.createFormGroup();  
		this.fileForm  = this.createFormGroupFile();
		this.consultaForm  = this.createFormConsulta();
		this.submitted = false; 
		this.btnSubir = false; 
	}

	createFormConsulta(){
		return new FormGroup({
			identification: new FormControl(),
			nombre: new FormControl(),
			nick: new FormControl(),
		}); 
	}

	onConsulta(){
		if(this.consultaForm.get('identification').value !== null){
			this.modeloTable  = this.modelos.filter(x => x.id === this.consultaForm.get('identification').value );
		}else if(this.consultaForm.get('nombre').value !== null){
			this.modeloTable  = this.modelos.filter(x => x.id === this.consultaForm.get('nombre').value );
		}else if(this.consultaForm.get('nick').value !== null){
			//this.modeloTable = this.modelos;
			let nickId = this.consultaForm.get('nick').value;
			let models:any[];
			this.modelos.forEach(function(a){
				a.nicks.forEach(function(b){
					if(b.id === nickId){
						models = a;
					} 
				})
			});
			if(models !== undefined){
				this.modeloTable = [models];
			}else{
				this.modeloTable = null;
			}
		}else{
			this.modeloTable = this.modelos;
		}
	}

	ChangeCargarArchivo(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
	}

	onEnviarArchivo(){  
		this.btnSubir = true; 
		if(!this.fileForm.invalid){ 
			this._modeloService.UploadFile('uploadsale', this.filesToUpload, this.fileForm.value);
			$('#btnCerrarModal').click();
			$('#alertMessage').html('<b>Carga de Ventas Éxitosa!</b>');
			$('#alertMessage').show();
			setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		}
		
	}

	createFormGroupFile(){
		return new FormGroup({
			file: new FormControl('',[Validators.required]),
			fecha_inicio: new FormControl('',[Validators.required]),
			fecha_fin: new FormControl('',[Validators.required]),
			page_id: new FormControl(null,[Validators.required])
		}); 
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			identification: new FormControl('',[Validators.required]),
			name: new FormControl('',[Validators.required]),
			lastname: new FormControl('',[Validators.required]),
			address: new FormControl(''),
			email: new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
			cellphone: new FormControl('',[Validators.required, Validators.maxLength(10)]),
			admission_date: new FormControl('',[Validators.required]),
			sede_id: new FormControl('',[Validators.required]),
			birth_date: new FormControl('',[Validators.required]),
			birth_country: new FormControl(''),
			birth_city: new FormControl(''),
			eps_id: new FormControl(''),
			afp_id: new FormControl(''),
			ccf_id: new FormControl(''),
			fdp_id: new FormControl(''),
			country_id: new FormControl(''),
			city_id: new FormControl(''),
			status_id: new FormControl(''),
			category_id: new FormControl(''),
			department: new FormControl(''),
			banco_id: new FormControl('',[Validators.required]),
			cuenta: new FormControl('',[Validators.required]),
			propietario: new FormControl(''),
			rut: new FormControl('',[Validators.required]),
		}); 
	}

	onResetForm(){
		this.textbtn ='Guardar';
		this.modeloForm.reset();
		this.modeloForm.controls['id'].setValue("");
	}

	onEditar(modelo:Modelo){  
		this.ciudFilter  = this.ciudades.filter(x => x.id === modelo.city_id);
		this.departFilter  = this.departamentos.filter(x => x.id === this.ciudFilter[0].parent_id);
		this.textbtn ='Modificar';
		this.modeloForm.controls['id'].setValue(modelo.id);
		this.modeloForm.controls['identification'].setValue(modelo.identification);
		this.modeloForm.controls['name'].setValue(modelo.name);
		this.modeloForm.controls['lastname'].setValue(modelo.lastname);
		this.modeloForm.controls['email'].setValue(modelo.email);
		this.modeloForm.controls['cellphone'].setValue(modelo.cellphone);
		this.modeloForm.controls['phone'].setValue(modelo.phone);
		this.modeloForm.controls['admission_date'].setValue(modelo.admission_date);
		this.modeloForm.controls['birth_date'].setValue(modelo.birth_date);
		this.modeloForm.controls['sede_id'].setValue(modelo.sede_id);
		this.modeloForm.controls['country_id'].setValue(modelo.country_id);
		this.modeloForm.controls['city_id'].setValue(modelo.city_id);
		this.modeloForm.controls['address'].setValue(modelo.address);
		this.modeloForm.controls['eps_id'].setValue(modelo.eps_id);
		this.modeloForm.controls['afp_id'].setValue(modelo.afp_id);
		this.modeloForm.controls['ccf_id'].setValue(modelo.ccf_id);
		this.modeloForm.controls['fdp_id'].setValue(modelo.fdp_id);
		this.modeloForm.controls['department'].setValue(this.ciudFilter[0].parent_id);
		this.modeloForm.controls['banco_id'].setValue(modelo.banco_id);
		this.modeloForm.controls['cuenta'].setValue(modelo.cuenta);
		this.modeloForm.controls['propietario'].setValue(modelo.propietario);
		this.modeloForm.controls['rut'].setValue(modelo.rut);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}
	
	onSaveForm(){
		this.submitted = true;
		if(this.modeloForm.get('id').value !== ''){
			if(!this.modeloForm .invalid){
				this.textbtn ='Procesando...';
				console.log(this.modeloForm.value)
				$('#btnGuardar').attr('disabled','true');
				this._modeloService.editItem(this.modeloForm.value,'models/editmodelo').subscribe(
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}  
		} else{  
			if(!this.modeloForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				console.log(this.modeloForm.value)
				this._modeloService.addItem(this.modeloForm.value,'models/modelos').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      },  
			      error => {
			        console.log(<any>error);
			      } 
			    ); 
			}
		}
	}

	onEliminar(id:string){
		if(confirm('¿Desea eliminar el registro?')){
			this._modeloService.getItems(id,'models/deletemodelo').subscribe(
				result => { 
					$('.loaderBox').hide();  
					this.getListas();
				},
				error => {
					console.log(<any>error);
				} 
			); 
		}
	}

	onChangeCiudad(e){
		this.modeloForm.controls['city_id'].setValue(null);
		if(e !== undefined){
			this.ciudFilter  = this.ciudades.filter(x => x.parent_id === e.id);
		}
	}

	onChangeDepartamento(e){ 
		this.modeloForm.controls['department'].setValue(null);
		if(e !== undefined){
			this.departFilter  = this.departamentos.filter(x => x.parent_id === e.id);
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._modeloService.getItems('0','models/modelos').subscribe(
			result => { 
				this.sedes = result.sedes;
				this.modelos = result.modelos;
				this.modeloTable = result.modelos;
				this.eps = result.eps;
				this.arl = result.arl;
				this.paises = result.paises;
				this.fp = result.fp;
				this.ciudades = result.ciudades;
				this.departamentos = result.departamentos;
				this.cdf = result.cdf;
				this.nicks = result.nicks;
				this.paginas = result.paginas;
				this.bancos = result.bancos; 
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				});
				$('.loaderBox').hide();
			}, 
			error => {
				console.log(<any>error);
			}
		); 

		
	}

	get f() { return this.modeloForm.controls; }
	get g() { return this.fileForm.controls; }
	get h() { return this.consultaForm.controls; }
}