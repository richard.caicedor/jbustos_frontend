import {Component, OnInit } from '@angular/core';
import { Modelo } from '../../to/modelo';
import { Nick } from '../../to/nick';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-nicks',
	templateUrl: './nicks.component.html',
	providers: [Servicio]
})

export class NicksComponent implements OnInit{

	public modelos:any[]; 
	public modelo:Modelo;
	public nicks:Nick[];
	public nicksSelect:any[];
	public nickForm:FormGroup;
	public textbtn:string;
	public submitted:boolean;
	public nombre:string;
	public paginas:any[];
	public consultaForm:FormGroup;
	public modeloTable:any[];

	constructor(public _service:Servicio){
		this.textbtn ='Guardar';
		this.nickForm  = this.createFormGroup();
		this.consultaForm  = this.createFormConsulta();
		this.submitted = false; 
	}

	createFormConsulta(){
		return new FormGroup({
			identification: new FormControl(),
			nombre: new FormControl(),
			nick: new FormControl(),
		});  
	}

	onConsulta(){
		if(this.consultaForm.get('identification').value !== null){
			this.modeloTable  = this.modelos.filter(x => x.id === this.consultaForm.get('identification').value );
		}else if(this.consultaForm.get('nombre').value !== null){
			this.modeloTable  = this.modelos.filter(x => x.id === this.consultaForm.get('nombre').value );
		}else if(this.consultaForm.get('nick').value !== null){
			//this.modeloTable = this.modelos;
			let nickId = this.consultaForm.get('nick').value;
			let models:any[];
			this.modelos.forEach(function(a){
				a.nicks.forEach(function(b){
					if(b.id === nickId){
						models = a;
					} 
				})
			});
			if(models !== undefined){
				this.modeloTable = [models];
			}else{
				this.modeloTable = null;
			}
		}else{
			this.modeloTable = this.modelos;
		}
	}
	
	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			nickname: new FormControl('',[Validators.required]),
			password: new FormControl('',[Validators.required]),
			url: new FormControl(''),
			page_id: new FormControl('',[Validators.required]),
			model_id: new FormControl('',[Validators.required])
		}); 
	} 

	onToggleForm(){
		this.onResetForm();
		$('#divForm').toggle(200);
	}
	
	onResetForm(){
		this.textbtn ='Guardar';
		this.nickForm.reset();
		this.nickForm.controls['id'].setValue("");
		this.nickForm.controls['page_id'].setValue("");
		this.nickForm.controls['model_id'].setValue(this.modelo.id);
	}

	onEditar(nick:Nick){
		$('#divForm').show(200);
		this.textbtn ='Modificar';
		this.nickForm.controls['id'].setValue(nick.id);
		this.nickForm.controls['nickname'].setValue(nick.nickname);
		this.nickForm.controls['password'].setValue(nick.password);
		this.nickForm.controls['url'].setValue(nick.url);
		this.nickForm.controls['page_id'].setValue(nick.page_id);
	}

	resultApi(msg:string){
		this.onToggleForm();
		$('#btnGuardar').removeAttr('disabled');
		this.submitted = false;
	}
	
	onSaveForm(){
		this.submitted = true;
		if(this.nickForm .get('id').value !== ''){
			if(!this.nickForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.editItem(this.nickForm.value,'nicks/editnick').subscribe(
			      response => {
			      	this.resultApi('<b>Modificación Éxitosa!</b>');
			      	this.onResetForm();
			        this.getListaNicks();
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}
		} else{
			if(!this.nickForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._service.addItem(this.nickForm.value,'nicks/nick').subscribe(
			      response => { 
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			        this.getListaNicks();
			      },   
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	cargarDatos(modelot:Modelo){
		this.modelo = modelot;
		this.nombre = modelot.name + ' '+modelot.lastname;  
		this.nickForm.controls['model_id'].setValue(modelot.id);  
		this.getListaNicks();
	}

	ngOnInit(){
		this.getListas();
	} 

	getListaNicks(){
		this._service.getItems(this.modelo.id,'nicks/nick').subscribe(
			result => { 
				this.nicks = result;
				$('.loaderBox').hide(); 
			},
			error => {
				console.log(<any>error);
			} 
		); 
	}

	onEliminar(nick:Nick){
		this._service.getItems(nick.id,'nicks/nickdelete').subscribe(
			result => { 
				$('.loaderBox').hide(); 
				this.getListaNicks();
			},
			error => {
				console.log(<any>error);
			} 
		); 
	}

	getListas(){
		this._service.getItems('0','models/modelos').subscribe(
			result => { 
				this.paginas = [{'id': null, 'name': '-- Seleccionar --'}];
				this.paginas = this.paginas.concat(result.paginas);
				this.modelos = result.modelos;
				this.modeloTable = result.modelos;
				this.nicksSelect = result.nicks;
				$('.loaderBox').hide(); 
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				});
			},
			error => {
				console.log(<any>error);
			} 
		); 
	}

	get f() { return this.nickForm.controls; }
}