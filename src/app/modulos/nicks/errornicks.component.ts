import {Component, OnInit } from '@angular/core';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-errornicks',
	templateUrl: './errornicks.component.html',
	providers: [Servicio]
}) 

export class ErrorNicksComponent implements OnInit{ 
 
	public titulo:string;
	public items:any[];
	public modelos:any[];
	public _formGroup:FormGroup;
	public textbtn:string;
	public submitted:boolean;
	public nicks:any[];
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.titulo = "Administración de Nicks no asociados";
		this._formGroup = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			nick: new FormControl('',[Validators.required]),
			model_id: new FormControl(null,[Validators.required]),
		}); 
	} 

	onResetForm(){  
		this.textbtn ='Guardar';
		this._formGroup.reset();
	} 

	onEditar(item){
		this._formGroup.controls['nick'].setValue(item.id);
	} 

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true; 
		console.log(this._formGroup.value); 
		/*
		if(!this._formGroup.invalid){ 
			this.textbtn ='Procesando...';
			$('#btnGuardar').attr('disabled','true');
			this._InformacionService.addItem(this._formGroup.value,'').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			    }, 
			    error => {
			      console.log(<any>error);
			    } 
			);
		}
		*/	
	} 

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this.items = [{ id:55, nick: 'Richard_15', ventas:'1500000'}]
		this.modelos = [{ nomcompleto: '1144164462 - Richard Caicedo', id: 5}]
		/*
		this._InformacionService.getItems('','').subscribe(
			result => {
				this.items = result;
				this.modelos.forEach(function(a){
					a.nomcompleto = a.name + ' ' + a.lastname; 
				})
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		);
		*/ 
	}

	get f() { return this._formGroup.controls; }
} 