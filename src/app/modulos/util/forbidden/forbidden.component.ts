import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-forbidden',
	templateUrl: './forbidden.component.html',
	styleUrls: ['./forbidden.component.css']
})
export class ForbiddenComponent implements OnInit {
	contentURL: string;
	permission: any;

	constructor(activated: ActivatedRoute) {
		activated.queryParams.subscribe(params => {
			this.contentURL = decodeURIComponent(params.url);
		});
	}

	ngOnInit() {}
}
