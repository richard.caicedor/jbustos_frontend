import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from 'src/app/guards';
import { MenuService } from 'src/app/services/api';

import { SharedModule } from '../shared/shared.module';
import { MenuFormComponent } from './form/menu-form.component';
import { MenuComponent } from './menu.component';

export const MenuRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: '',
				canActivate: [PermissionGuard],
				component: MenuComponent,
				data: {
					title: 'Administración de Menú',
					permission: 'menu_view'
				}
			},
			{
				path: 'add',
				component: MenuFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Agregar Menú',
					permission: 'menu_create'
				}
			},
			{
				path: 'edit/:id',
				component: MenuFormComponent,
				canActivate: [PermissionGuard],
				data: {
					title: 'Editar Menú',
					permission: 'menu_edit'
				}
			}
		]
	}
];

@NgModule({
	imports: [CommonModule, RouterModule.forChild(MenuRoutes), SharedModule],
	declarations: [MenuComponent, MenuFormComponent],
	providers: [MenuService]
})
export class MenuModule {}
