import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/services/api';
import { MessageType, PagerService, SwalMessageService } from 'src/app/services/util';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss'],
	providers: [PagerService]
})
export class MenuComponent implements OnInit {
	constructor(private menuService: MenuService, private swal: SwalMessageService, public pager: PagerService) {}

	ngOnInit() {}

	/**
	 * Mostrar listado
	 */
	getData() {
		this.pager.loading = true;
		this.menuService.index(this.pager.getParams(), {
			success: res => {
				this.pager.rows = res.menus;
				this.pager.count = res.count;
			},
			always: () => (this.pager.loading = false)
		});
	}

	/**
	 * Borrar
	 *
	 * @param row
	 */
	delete(row: any): void {
		this.swal.confirmationAjax('Eiminar Menú', 'Esta seguro que desea eliminar el menú?', {
			ok: {
				text: 'Eliminar',
				handler: () => {
					this.menuService.delete(row.id, {
						success: () => {
							this.swal.success('Eliminado', 'Menú eliminado');
							this.getData();
						},
						errorMsgType: MessageType.Swal
					});
				}
			}
		});
	}
}
