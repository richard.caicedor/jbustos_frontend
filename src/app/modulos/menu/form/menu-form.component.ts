import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ActionsService, MenuService } from 'src/app/services/api';
import { NotifyMessageService, ReturnPagerService } from 'src/app/services/util';

import { CustomValidators } from '../../shared';
import { SelectOptions } from '../../shared/select';

@Component({
	selector: 'app-menu-form',
	templateUrl: './menu-form.component.html',
	styleUrls: ['./menu-form.component.scss'],
	providers: [ReturnPagerService, ActionsService]
})
export class MenuFormComponent implements OnInit {
	loading: boolean;
	form: FormGroup;

	list = {
		menus: new SelectOptions(this.menuService.list),
		actions: new SelectOptions(this.actionsService.list)
	};

	constructor(
		fb: FormBuilder,
		private menuService: MenuService,
		private actionsService: ActionsService,
		private notify: NotifyMessageService,
		public returnPager: ReturnPagerService,
		activated: ActivatedRoute
	) {
		this.list.menus.fetch();
		this.list.actions.fetch();

		this.returnPager.path = ['/menu'];

		this.form = fb.group({
			id: [null],
			name: [null, [Validators.required, Validators.maxLength(45)]],
			url: [null, [Validators.maxLength(100)]],
			icon: [null, [Validators.maxLength(45)]],
			order: [null, [Validators.required, CustomValidators.integer]],
			menu_id_parent: [null],
			action_id: [null]
		});

		activated.params.subscribe(params => {
			const id = +params.id;
			if (!id) return;

			this.loading = true;
			menuService.get(id, {
				success: res => this.form.patchValue(res.menu),
				always: () => (this.loading = false)
			});
		});
	}

	ngOnInit() {}

	save() {
		if (!this.form.validate()) return;

		this.loading = true;
		this.menuService.save(this.form.value, {
			success: () => {
				this.notify.success('Exito', 'Menú guardado');
				this.returnPager.back();
			},
			always: () => (this.loading = false)
		});
	}
}
