import {Component, OnInit } from '@angular/core';
import {Informacion} from '../../to/informacion';
import {Servicio} from '../../services/servicios.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as $ from "jquery";

@Component({
	selector: 'app-pensiones',
	templateUrl: './pensiones.component.html',
	providers: [Servicio]
}) 

export class PensionesComponent implements OnInit{

	public items:Informacion[];
	public pensionesForm:FormGroup;
	public textbtn:string;
	submitted:boolean; 
 
	constructor(public _InformacionService:Servicio){ 
		this.textbtn ='Guardar';
		this.pensionesForm = this.createFormGroup();
		this.submitted = false;
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			description: new FormControl('',[Validators.required]),
			category_id: new FormControl(''),
		}); 
	} 

	onResetForm(){
		this.textbtn ='Guardar';
		this.pensionesForm.reset();
		this.pensionesForm.controls['id'].setValue(""); 
	} 

	onEditar(item:Informacion){
		this.textbtn ='Modificar';
		this.pensionesForm.controls['id'].setValue(item.id);
		this.pensionesForm.controls['description'].setValue(item.description);
		this.pensionesForm.controls['category_id'].setValue(25);
	}

	resultApi(msg:string){
		$('#alertMessage').show();
		$('#alertMessage').html(msg);
		$('#btnCerrar').click();
		this.getListas();
		$('#btnGuardar').removeAttr('disabled');
		setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
		this.submitted = false;
	}

	onSaveForm(){
		this.submitted = true;
		if(this.pensionesForm.get('id').value !== ''){
			if(!this.pensionesForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._InformacionService.editItem(this.pensionesForm.value,'informacion/editbasicinfo').subscribe(
			      response => {
			      	this.resultApi("<b>Modificación Éxitosa!</b>");
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			} 
		} else{ 
			if(!this.pensionesForm.invalid){ 
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this.pensionesForm.controls['category_id'].setValue(25); 
				this._InformacionService.addItem(this.pensionesForm.value,'informacion/basicinfo').subscribe(
			      response => {
			      	this.resultApi('<b>Grabación Éxitosa!</b>');
			        this.onResetForm();
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}

	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._InformacionService.getItems('25','informacion/basicinfo').subscribe( 
			result => {
				this.items = result;
				 $('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 
	}

	get f() { return this.pensionesForm.controls; }
}