import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';

import { Servicio } from '../../services/servicios.service';
import { Categoria } from '../../to/categoria';
import { Pagina } from '../../to/pagina';

@Component({
	selector: 'app-paginas',
	templateUrl: './paginas.component.html',
	providers: [Servicio]
})

export class PaginasComponent implements OnInit{

	public titulo:string;
	public paginas:Pagina[]; 
	public categorias:Categoria[];
	paginaForm:FormGroup;
	public textbtn:string;
	submitted:boolean;

	constructor(public _categoriaService:Servicio){
		this.textbtn ='Guardar';
		this.paginaForm = this.createFormGroup();
		this.submitted = false; 
	}

	createFormGroup(){
		return new FormGroup({
			id: new FormControl(''),
			name: new FormControl('',[Validators.required]),
			url: new FormControl('',[Validators.required]),
			url_login: new FormControl('',[Validators.required]),
			column_nick: new FormControl('',[Validators.required, Validators.maxLength(3)]),
			category_id: new FormControl('',[Validators.required]),
			column_value: new FormControl('',[Validators.required, Validators.maxLength(3)]),
			column_bonus: new FormControl('',[Validators.required , Validators.maxLength(3)])
		}); 
	}

	onResetForm(){
		this.textbtn ='Guardar';
		this.paginaForm.reset();
		this.paginaForm.controls['id'].setValue("");
		this.paginaForm.controls['category_id'].setValue("");
	}

	onEditar(pagina:Pagina){
		this.textbtn ='Modificar';
		this.paginaForm.controls['id'].setValue(pagina.id);
		this.paginaForm.controls['name'].setValue(pagina.name);
		this.paginaForm.controls['url'].setValue(pagina.url);
		this.paginaForm.controls['url_login'].setValue(pagina.url_login);
		this.paginaForm.controls['column_nick'].setValue(pagina.column_nick);
		this.paginaForm.controls['column_value'].setValue(pagina.column_value);
		this.paginaForm.controls['category_id'].setValue(pagina.category_id);
		this.paginaForm.controls['column_bonus'].setValue(pagina.column_bonus);
	}

	onSaveForm(){
		this.submitted = true;
		if(this.paginaForm.get('id').value !== ''){
			if(!this.paginaForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._categoriaService.editItem(this.paginaForm.value,'pages/editpage').subscribe(
			      response => {
			      	$('#alertMessage').show();
			      	$('#alertMessage').html('<b>Modificación Éxitosa!</b>');
			        $('#btnCerrar').click();
			        this.getListas();
			        $('#btnGuardar').removeAttr('disabled');
			        setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
			        this.submitted = false;
			      }, 
			      error => { 
			        console.log(<any>error);
			      } 
			    );
			}
		} else{
			if(!this.paginaForm.invalid){
				this.textbtn ='Procesando...';
				$('#btnGuardar').attr('disabled','true');
				this._categoriaService.addItem(this.paginaForm.value,'pages/page').subscribe(
			      response => {
			      	$('#alertMessage').show();
			      	$('#alertMessage').html('<b>Grabación Éxitosa!</b>');
			      	$('#btnGuardar').removeAttr('disabled');
			        $('#btnCerrar').click();
			        this.onResetForm();
			        this.getListas();
			        this.submitted = false;
			        setTimeout(function(){ $('#alertMessage').fadeOut(500); }, 2000);
			      }, 
			      error => {
			        console.log(<any>error);
			      } 
			    );
			}
		}
	}
  
	ngOnInit(){
		this.getListas();
	}

	getListas(){
		this._categoriaService.getItems('p','pages/categorybytype').subscribe(
			result => {
				this.categorias = [{'id': null, 'description': '-- Seleccionar --', 'type': 'p'}];
				this.categorias = this.categorias.concat(result.categorias);
				this.paginas = result.paginas;
				$('.loaderBox').hide();
			},
			error => {
				console.log(<any>error);
			}
		); 

		
	}

	get f() { return this.paginaForm.controls; }
}