import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';

const AuthRoutes: Routes = [
	{
		path: 'login',
		component: LoginComponent,
		data: {
			title: 'Iniciar Sesión'
		}
	}
	//   {
	//     path: 'recover-password',
	//     component: PasswordResetComponent
	//   },
	//   {
	//     path: 'new-password',
	//     component: NewPasswordComponent
	//   }
];
@NgModule({
	imports: [CommonModule, RouterModule.forChild(AuthRoutes), SharedModule],
	declarations: [LoginComponent]
})
export class AuthModule {}
