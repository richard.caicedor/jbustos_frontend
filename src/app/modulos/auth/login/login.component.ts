import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/api';
import { MessageType, NotifyMessageService, SessionService } from 'src/app/services/util';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	providers: [UsersService]
})
export class LoginComponent implements OnInit {
	loading: boolean;
	loginForm: FormGroup;
	return = '';

	constructor(
		private activated: ActivatedRoute,
		private userService: UsersService,
		private router: Router,
		fb: FormBuilder,
		private notify: NotifyMessageService,
		private sessionService: SessionService
	) {
		this.loginForm = fb.group({
			nick: [null, [Validators.required]],
			password: [null, [Validators.required]]
		});
	}

	ngOnInit() {
		$('.loaderBox').hide();
		this.activated.queryParams.subscribe(params => (this.return = params.return));
	}

	onLogin() {
		if (!this.loginForm.invalid) {
			this.loading = true;
			this.userService.login(this.loginForm.value, {
				success: res => {
					this.sessionService.setUser(res.user, res.actions);
					this.sessionService.setToken(res.token, res.timestamp);

					const url = decodeURIComponent(this.return);
					if (this.return) this.router.navigateByUrl(url);
					else this.router.navigate(['/']);
				},
				errorMsgType: MessageType.None,
				error: (res, util) => {
					switch (res.type) {
						case 'login_failed':
							this.notify.error('Error', 'Nombre de usuario o contraseña incorrectos');
							break;
						default:
							util.showNotifyError();
							break;
					}
				},
				always: () => (this.loading = false)
			});
		}
	}
}
