export const environment = {
	production: false,
	testing: false,
	URL: {
		WEB: 'http://localhost:8000/',
		API: 'http://localhost:8000/api/'
	}
};
