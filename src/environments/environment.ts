export const environment = {
	production: false,
	testing: false,
	URL: {
		WEB: 'http://localhost:8001/',
		API: 'http://localhost:8001/api/'
	}
};
